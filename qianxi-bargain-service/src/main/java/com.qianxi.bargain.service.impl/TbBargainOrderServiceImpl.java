package com.qianxi.bargain.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qianxi.bargain.service.TbBargainOrderService;
import com.qianxi.entity.PageResult;
import com.qianxi.mapper.TbBargainOrderMapper;
import com.qianxi.pojo.TbBargainOrder;
import com.qianxi.pojo.TbBargainOrderExample;


import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbBargainOrderServiceImpl implements TbBargainOrderService {

    @Resource
    private TbBargainOrderMapper tbBargainOrderMapper;

    @Override
    public List<TbBargainOrder> findAll() {
        return tbBargainOrderMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbBargainOrder> pageList(TbBargainOrder tbBargainOrder, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbBargainOrderExample example = new TbBargainOrderExample();
        TbBargainOrderExample.Criteria criteria = example.createCriteria();
        if(tbBargainOrder !=null){

        }

        Page<TbBargainOrder> page = (Page<TbBargainOrder>) tbBargainOrderMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbBargainOrder tbBargainOrder) {
        Long id =tbBargainOrder.getId();
        if(id == null || id == 0){
            tbBargainOrderMapper.insert(tbBargainOrder);
        }
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbBargainOrderMapper.deleteByExample(id);
        }
    }

    @Override
    public TbBargainOrder getById(Long id) {
        return (TbBargainOrder) tbBargainOrderMapper.selectByExample(null);
    }
}
