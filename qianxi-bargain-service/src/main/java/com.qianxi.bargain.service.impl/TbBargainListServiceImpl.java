package com.qianxi.bargain.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qianxi.entity.PageResult;
import com.qianxi.mapper.TbBargainListMapper;
import com.qianxi.bargain.service.TbBargainListService;
import com.qianxi.pojo.TbBargainList;
import com.qianxi.pojo.TbBargainListExample;


import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbBargainListServiceImpl implements TbBargainListService {

    @Resource
    private TbBargainListMapper tbBargainListMapper;

    @Override
    public List<TbBargainList> findAll() {
        return tbBargainListMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbBargainList> pageList(TbBargainList tbBargainList, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbBargainListExample example = new TbBargainListExample();
        TbBargainListExample.Criteria criteria = example.createCriteria();
        if(tbBargainList !=null){
            if (tbBargainList.getStatus()!=null){
                criteria.andStatusEqualTo(tbBargainList.getStatus());
            }
        }

        Page<TbBargainList> page = (Page<TbBargainList>) tbBargainListMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbBargainList tbBargainList) {
        tbBargainListMapper.insert(tbBargainList);
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbBargainListMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbBargainList getById(Long id) {
        return tbBargainListMapper.selectByPrimaryKey(id);
    }
}
