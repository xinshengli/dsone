package com.qianxi.bargain.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qianxi.bargain.service.TbBargainRecordService;
import com.qianxi.entity.PageResult;
import com.qianxi.mapper.TbBargainListMapper;
import com.qianxi.mapper.TbBargainRecordMapper;
import com.qianxi.pojo.TbBargainList;
import com.qianxi.pojo.TbBargainListExample;
import com.qianxi.pojo.TbBargainRecord;
import com.qianxi.pojo.TbBargainRecordExample;

import javax.annotation.Resource;
import java.util.List;
@Service(timeout = 5000,retries = 0)
public class TbBargainRecordServiceImpl implements TbBargainRecordService {
    @Resource
    private TbBargainRecordMapper tbBargainRecordMapper;

    @Override
    public List<TbBargainRecord> findAll() {
        return tbBargainRecordMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbBargainRecord> pageList(TbBargainRecord tbBargainRecord, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbBargainRecordExample example = new TbBargainRecordExample();
        TbBargainRecordExample.Criteria criteria = example.createCriteria();
        if(tbBargainRecord !=null){

        }

        Page<TbBargainRecord> page = (Page<TbBargainRecord>) tbBargainRecordMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbBargainRecord tbBargainRecord) {
        tbBargainRecordMapper.insert(tbBargainRecord);
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbBargainRecordMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbBargainRecord getById(Long id) {
        return tbBargainRecordMapper.selectByPrimaryKey(id);
    }

    @Override
    public TbBargainRecord getkanjia(Long barId) {
        return tbBargainRecordMapper.kanjia(barId);
    }
}
