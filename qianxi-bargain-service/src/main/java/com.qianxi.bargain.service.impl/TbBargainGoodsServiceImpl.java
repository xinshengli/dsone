package com.qianxi.bargain.service.impl;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qianxi.entity.PageResult;
import com.qianxi.mapper.TbBargainGoodsMapper;
import com.qianxi.pojo.TbBargainGoods;
import com.qianxi.pojo.TbBargainGoodsExample;
import com.qianxi.bargain.service.TbBargainGoodsService;

import javax.annotation.Resource;
import java.util.List;
@Service(timeout = 5000,retries = 0)
public class TbBargainGoodsServiceImpl implements TbBargainGoodsService {
    @Resource
    private TbBargainGoodsMapper tbBargainGoodsMapper;
    @Override
    public List<TbBargainGoods> findAll() {
        return tbBargainGoodsMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbBargainGoods> pageList(TbBargainGoods tbBargainGoods, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbBargainGoodsExample example = new TbBargainGoodsExample();
        TbBargainGoodsExample.Criteria criteria = example.createCriteria();
        if (tbBargainGoods!=null){
            if (StringUtils.isNotEmpty(tbBargainGoods.getBarStatus())){
                criteria.andBarStatusEqualTo(tbBargainGoods.getBarStatus());
            }
            if (tbBargainGoods.getGoodsId()!=null){
                criteria.andGoodsIdEqualTo(tbBargainGoods.getGoodsId());
            }
        }

        Page<TbBargainGoods> page = (Page<TbBargainGoods>) tbBargainGoodsMapper.selectByExample(example);
        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbBargainGoods tbBargainGoods) {
        Long id = tbBargainGoods.getBarGoodsId();
        if(id == null || id == 0){
            tbBargainGoodsMapper.insert(tbBargainGoods);
        } else{
            tbBargainGoodsMapper.updateByPrimaryKey(tbBargainGoods);
        }

    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbBargainGoodsMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbBargainGoods getById(Long id) {
        return tbBargainGoodsMapper.selectByPrimaryKey(id);
    }
}
