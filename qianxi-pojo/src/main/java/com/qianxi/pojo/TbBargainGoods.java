package com.qianxi.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TbBargainGoods implements Serializable {
    private Long barGoodsId;

    private String batImg;

    private Integer goodsId;

    private String barName;

    private String barPrice;

    private Integer barMoneyMin;

    private Integer barPeopleNum;

    private Integer bmbarPeopleNum;

    private Integer seccessPeopleNum;

    private Integer goodsXianliang;

    private Integer goodsShengyu;

    private Date startTime;

    private Date endTime;

    private String barStatus;

    private Integer barYouxiaoTime;

    private String barGoodsContent;

    public Long getBarGoodsId() {
        return barGoodsId;
    }

    public void setBarGoodsId(Long barGoodsId) {
        this.barGoodsId = barGoodsId;
    }

    public String getBatImg() {
        return batImg;
    }

    public void setBatImg(String batImg) {
        this.batImg = batImg == null ? null : batImg.trim();
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getBarName() {
        return barName;
    }

    public void setBarName(String barName) {
        this.barName = barName == null ? null : barName.trim();
    }

    public String getBarPrice() {
        return barPrice;
    }

    public void setBarPrice(String barPrice) {
        this.barPrice = barPrice;
    }

    public Integer getBarMoneyMin() {
        return barMoneyMin;
    }

    public void setBarMoneyMin(Integer barMoneyMin) {
        this.barMoneyMin = barMoneyMin;
    }

    public Integer getBarPeopleNum() {
        return barPeopleNum;
    }

    public void setBarPeopleNum(Integer barPeopleNum) {
        this.barPeopleNum = barPeopleNum;
    }

    public Integer getBmbarPeopleNum() {
        return bmbarPeopleNum;
    }

    public void setBmbarPeopleNum(Integer bmbarPeopleNum) {
        this.bmbarPeopleNum = bmbarPeopleNum;
    }

    public Integer getSeccessPeopleNum() {
        return seccessPeopleNum;
    }

    public void setSeccessPeopleNum(Integer seccessPeopleNum) {
        this.seccessPeopleNum = seccessPeopleNum;
    }

    public Integer getGoodsXianliang() {
        return goodsXianliang;
    }

    public void setGoodsXianliang(Integer goodsXianliang) {
        this.goodsXianliang = goodsXianliang;
    }

    public Integer getGoodsShengyu() {
        return goodsShengyu;
    }

    public void setGoodsShengyu(Integer goodsShengyu) {
        this.goodsShengyu = goodsShengyu;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getBarStatus() {
        return barStatus;
    }

    public void setBarStatus(String barStatus) {
        this.barStatus = barStatus == null ? null : barStatus.trim();
    }

    public Integer getBarYouxiaoTime() {
        return barYouxiaoTime;
    }

    public void setBarYouxiaoTime(Integer barYouxiaoTime) {
        this.barYouxiaoTime = barYouxiaoTime;
    }

    public String getBarGoodsContent() {
        return barGoodsContent;
    }

    public void setBarGoodsContent(String barGoodsContent) {
        this.barGoodsContent = barGoodsContent == null ? null : barGoodsContent.trim();
    }
}