package com.qianxi.pojo;

import java.io.Serializable;
import java.util.Date;

public class TbBargainRecord implements Serializable {
    private Long id;

    private String userId;

    private Integer barId;

    private Integer goodsId;

    private String barPrice;

    private String barTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Integer getBarId() {
        return barId;
    }

    public void setBarId(Integer barId) {
        this.barId = barId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getBarPrice() {
        return barPrice;
    }

    public void setBarPrice(String barPrice) {
        this.barPrice = barPrice;
    }

    public String getBarTime() {
        return barTime;
    }

    public void setBarTime(String barTime) {
        this.barTime = barTime;
    }
}