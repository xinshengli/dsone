package com.qianxi.pojo;

import java.io.Serializable;
import java.util.Date;

public class TbComment implements Serializable {
    private Long id;

    private Long goodsId;

    private Long itemId;

    private Long userId;

    private Integer goodsFen;

    private Integer fuwuFen;

    private String content;

    private String huifu;

    private Date commentTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getGoodsFen() {
        return goodsFen;
    }

    public void setGoodsFen(Integer goodsFen) {
        this.goodsFen = goodsFen;
    }

    public Integer getFuwuFen() {
        return fuwuFen;
    }

    public void setFuwuFen(Integer fuwuFen) {
        this.fuwuFen = fuwuFen;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getHuifu() {
        return huifu;
    }

    public void setHuifu(String huifu) {
        this.huifu = huifu == null ? null : huifu.trim();
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }
}