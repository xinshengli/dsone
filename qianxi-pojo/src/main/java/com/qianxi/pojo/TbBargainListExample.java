package com.qianxi.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TbBargainListExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TbBargainListExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdIsNull() {
            addCriterion("bar_goods_id is null");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdIsNotNull() {
            addCriterion("bar_goods_id is not null");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdEqualTo(Integer value) {
            addCriterion("bar_goods_id =", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdNotEqualTo(Integer value) {
            addCriterion("bar_goods_id <>", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdGreaterThan(Integer value) {
            addCriterion("bar_goods_id >", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("bar_goods_id >=", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdLessThan(Integer value) {
            addCriterion("bar_goods_id <", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdLessThanOrEqualTo(Integer value) {
            addCriterion("bar_goods_id <=", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdIn(List<Integer> values) {
            addCriterion("bar_goods_id in", values, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdNotIn(List<Integer> values) {
            addCriterion("bar_goods_id not in", values, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdBetween(Integer value1, Integer value2) {
            addCriterion("bar_goods_id between", value1, value2, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdNotBetween(Integer value1, Integer value2) {
            addCriterion("bar_goods_id not between", value1, value2, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarNumIsNull() {
            addCriterion("bar_num is null");
            return (Criteria) this;
        }

        public Criteria andBarNumIsNotNull() {
            addCriterion("bar_num is not null");
            return (Criteria) this;
        }

        public Criteria andBarNumEqualTo(Integer value) {
            addCriterion("bar_num =", value, "barNum");
            return (Criteria) this;
        }

        public Criteria andBarNumNotEqualTo(Integer value) {
            addCriterion("bar_num <>", value, "barNum");
            return (Criteria) this;
        }

        public Criteria andBarNumGreaterThan(Integer value) {
            addCriterion("bar_num >", value, "barNum");
            return (Criteria) this;
        }

        public Criteria andBarNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("bar_num >=", value, "barNum");
            return (Criteria) this;
        }

        public Criteria andBarNumLessThan(Integer value) {
            addCriterion("bar_num <", value, "barNum");
            return (Criteria) this;
        }

        public Criteria andBarNumLessThanOrEqualTo(Integer value) {
            addCriterion("bar_num <=", value, "barNum");
            return (Criteria) this;
        }

        public Criteria andBarNumIn(List<Integer> values) {
            addCriterion("bar_num in", values, "barNum");
            return (Criteria) this;
        }

        public Criteria andBarNumNotIn(List<Integer> values) {
            addCriterion("bar_num not in", values, "barNum");
            return (Criteria) this;
        }

        public Criteria andBarNumBetween(Integer value1, Integer value2) {
            addCriterion("bar_num between", value1, value2, "barNum");
            return (Criteria) this;
        }

        public Criteria andBarNumNotBetween(Integer value1, Integer value2) {
            addCriterion("bar_num not between", value1, value2, "barNum");
            return (Criteria) this;
        }

        public Criteria andBarResidueIsNull() {
            addCriterion("bar_residue is null");
            return (Criteria) this;
        }

        public Criteria andBarResidueIsNotNull() {
            addCriterion("bar_residue is not null");
            return (Criteria) this;
        }

        public Criteria andBarResidueEqualTo(Integer value) {
            addCriterion("bar_residue =", value, "barResidue");
            return (Criteria) this;
        }

        public Criteria andBarResidueNotEqualTo(Integer value) {
            addCriterion("bar_residue <>", value, "barResidue");
            return (Criteria) this;
        }

        public Criteria andBarResidueGreaterThan(Integer value) {
            addCriterion("bar_residue >", value, "barResidue");
            return (Criteria) this;
        }

        public Criteria andBarResidueGreaterThanOrEqualTo(Integer value) {
            addCriterion("bar_residue >=", value, "barResidue");
            return (Criteria) this;
        }

        public Criteria andBarResidueLessThan(Integer value) {
            addCriterion("bar_residue <", value, "barResidue");
            return (Criteria) this;
        }

        public Criteria andBarResidueLessThanOrEqualTo(Integer value) {
            addCriterion("bar_residue <=", value, "barResidue");
            return (Criteria) this;
        }

        public Criteria andBarResidueIn(List<Integer> values) {
            addCriterion("bar_residue in", values, "barResidue");
            return (Criteria) this;
        }

        public Criteria andBarResidueNotIn(List<Integer> values) {
            addCriterion("bar_residue not in", values, "barResidue");
            return (Criteria) this;
        }

        public Criteria andBarResidueBetween(Integer value1, Integer value2) {
            addCriterion("bar_residue between", value1, value2, "barResidue");
            return (Criteria) this;
        }

        public Criteria andBarResidueNotBetween(Integer value1, Integer value2) {
            addCriterion("bar_residue not between", value1, value2, "barResidue");
            return (Criteria) this;
        }

        public Criteria andUserAddressIsNull() {
            addCriterion("user_address is null");
            return (Criteria) this;
        }

        public Criteria andUserAddressIsNotNull() {
            addCriterion("user_address is not null");
            return (Criteria) this;
        }

        public Criteria andUserAddressEqualTo(String value) {
            addCriterion("user_address =", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressNotEqualTo(String value) {
            addCriterion("user_address <>", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressGreaterThan(String value) {
            addCriterion("user_address >", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressGreaterThanOrEqualTo(String value) {
            addCriterion("user_address >=", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressLessThan(String value) {
            addCriterion("user_address <", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressLessThanOrEqualTo(String value) {
            addCriterion("user_address <=", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressLike(String value) {
            addCriterion("user_address like", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressNotLike(String value) {
            addCriterion("user_address not like", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressIn(List<String> values) {
            addCriterion("user_address in", values, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressNotIn(List<String> values) {
            addCriterion("user_address not in", values, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressBetween(String value1, String value2) {
            addCriterion("user_address between", value1, value2, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressNotBetween(String value1, String value2) {
            addCriterion("user_address not between", value1, value2, "userAddress");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("start_time is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("start_time is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Date value) {
            addCriterion("start_time =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Date value) {
            addCriterion("start_time <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Date value) {
            addCriterion("start_time >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("start_time >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Date value) {
            addCriterion("start_time <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("start_time <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Date> values) {
            addCriterion("start_time in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Date> values) {
            addCriterion("start_time not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Date value1, Date value2) {
            addCriterion("start_time between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("start_time not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("end_time is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("end_time is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Date value) {
            addCriterion("end_time =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Date value) {
            addCriterion("end_time <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Date value) {
            addCriterion("end_time >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("end_time >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Date value) {
            addCriterion("end_time <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("end_time <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Date> values) {
            addCriterion("end_time in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Date> values) {
            addCriterion("end_time not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Date value1, Date value2) {
            addCriterion("end_time between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("end_time not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andBarPriceIsNull() {
            addCriterion("bar_price is null");
            return (Criteria) this;
        }

        public Criteria andBarPriceIsNotNull() {
            addCriterion("bar_price is not null");
            return (Criteria) this;
        }

        public Criteria andBarPriceEqualTo(BigDecimal value) {
            addCriterion("bar_price =", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceNotEqualTo(BigDecimal value) {
            addCriterion("bar_price <>", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceGreaterThan(BigDecimal value) {
            addCriterion("bar_price >", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("bar_price >=", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceLessThan(BigDecimal value) {
            addCriterion("bar_price <", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("bar_price <=", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceIn(List<BigDecimal> values) {
            addCriterion("bar_price in", values, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceNotIn(List<BigDecimal> values) {
            addCriterion("bar_price not in", values, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bar_price between", value1, value2, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bar_price not between", value1, value2, "barPrice");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("user_id like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("user_id not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}