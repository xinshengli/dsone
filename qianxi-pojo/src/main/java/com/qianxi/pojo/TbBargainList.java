package com.qianxi.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TbBargainList implements Serializable {
    private Long id;

    private Integer barGoodsId;

    private Integer barNum;

    private Integer barResidue;

    private String userAddress;

    private String startTime;

    private String endTime;

    private String status;

    private String barPrice;

    private String userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBarGoodsId() {
        return barGoodsId;
    }

    public void setBarGoodsId(Integer barGoodsId) {
        this.barGoodsId = barGoodsId;
    }

    public Integer getBarNum() {
        return barNum;
    }

    public void setBarNum(Integer barNum) {
        this.barNum = barNum;
    }

    public Integer getBarResidue() {
        return barResidue;
    }

    public void setBarResidue(Integer barResidue) {
        this.barResidue = barResidue;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress == null ? null : userAddress.trim();
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getBarPrice() {
        return barPrice;
    }

    public void setBarPrice(String barPrice) {
        this.barPrice = barPrice;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }
}