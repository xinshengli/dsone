package com.qianxi.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TbKillGoods implements Serializable {
    private Long id;

    private Integer goodsId;

    private Integer itemId;

    private String tupianPath;

    private BigDecimal price;

    private Integer salesnum;

    private Integer stock;

    private Integer paixu;

    private Integer status;

    private String operationTime;

    private Integer xianliang;

    private Long killChangId;

    private String killContent;

    private String creatTime;

    private String killprice;

    public String getKillPrice() {
        return killprice;
    }

    public void setKillPrice(String killPrice) {
        this.killprice = killPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getTupianPath() {
        return tupianPath;
    }

    public void setTupianPath(String tupianPath) {
        this.tupianPath = tupianPath == null ? null : tupianPath.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getSalesnum() {
        return salesnum;
    }

    public void setSalesnum(Integer salesnum) {
        this.salesnum = salesnum;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getPaixu() {
        return paixu;
    }

    public void setPaixu(Integer paixu) {
        this.paixu = paixu;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(String operationTime) {
        this.operationTime = operationTime;
    }

    public Integer getXianliang() {
        return xianliang;
    }

    public void setXianliang(Integer xianliang) {
        this.xianliang = xianliang;
    }

    public Long getKillChangId() {
        return killChangId;
    }

    public void setKillChangId(Long killChangId) {
        this.killChangId = killChangId;
    }

    public String getKillContent() {
        return killContent;
    }

    public void setKillContent(String killContent) {
        this.killContent = killContent == null ? null : killContent.trim();
    }

    public String getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(String creatTime) {
        this.creatTime = creatTime;
    }

    @Override
    public String toString() {
        return "TbKillGoods{" +
                "id=" + id +
                ", goodsId=" + goodsId +
                ", itemId=" + itemId +
                ", tupianPath='" + tupianPath + '\'' +
                ", price=" + price +
                ", salesnum=" + salesnum +
                ", stock=" + stock +
                ", paixu=" + paixu +
                ", status=" + status +
                ", operationTime='" + operationTime + '\'' +
                ", xianliang=" + xianliang +
                ", killChangId=" + killChangId +
                ", killContent='" + killContent + '\'' +
                ", creatTime='" + creatTime + '\'' +
                ", killPrice='" + killprice + '\'' +
                '}';
    }
}