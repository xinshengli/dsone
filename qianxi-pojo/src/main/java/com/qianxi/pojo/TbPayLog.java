package com.qianxi.pojo;

import java.io.Serializable;

public class TbPayLog implements Serializable{
     private String outTradeNo;//支付订单号

     private String createTime;//创建日期

     private String payTime;//支付完成时间

     private Long totalFee;//支付金额（分）

     private String userId;//用户ID

     private String transactionId;//交易号码

     private String tradeState;//交易状态

     private String orderList;//订单编号列表

     private String payType;//支付类型


    public void setOutTradeNo(String outTradeNo){
        this.outTradeNo=outTradeNo;
    }

    public String getOutTradeNo(){
        return outTradeNo;
    }

    public void setCreateTime(String createTime){
        this.createTime=createTime;
    }

    public String getCreateTime(){
        return createTime;
    }

    public void setPayTime(String payTime){
        this.payTime=payTime;
    }

    public String getPayTime(){
        return payTime;
    }

    public void setTotalFee(Long totalFee){
        this.totalFee=totalFee;
    }

    public Long getTotalFee(){
        return totalFee;
    }

    public void setUserId(String userId){
        this.userId=userId;
    }

    public String getUserId(){
        return userId;
    }

    public void setTransactionId(String transactionId){
        this.transactionId=transactionId;
    }

    public String getTransactionId(){
        return transactionId;
    }

    public void setTradeState(String tradeState){
        this.tradeState=tradeState;
    }

    public String getTradeState(){
        return tradeState;
    }

    public void setOrderList(String orderList){
        this.orderList=orderList;
    }

    public String getOrderList(){
        return orderList;
    }

    public void setPayType(String payType){
        this.payType=payType;
    }

    public String getPayType(){
        return payType;
    }

}