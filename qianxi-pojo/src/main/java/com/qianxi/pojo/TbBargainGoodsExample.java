package com.qianxi.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TbBargainGoodsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TbBargainGoodsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBarGoodsIdIsNull() {
            addCriterion("bar_goods_id is null");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdIsNotNull() {
            addCriterion("bar_goods_id is not null");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdEqualTo(Long value) {
            addCriterion("bar_goods_id =", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdNotEqualTo(Long value) {
            addCriterion("bar_goods_id <>", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdGreaterThan(Long value) {
            addCriterion("bar_goods_id >", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdGreaterThanOrEqualTo(Long value) {
            addCriterion("bar_goods_id >=", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdLessThan(Long value) {
            addCriterion("bar_goods_id <", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdLessThanOrEqualTo(Long value) {
            addCriterion("bar_goods_id <=", value, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdIn(List<Long> values) {
            addCriterion("bar_goods_id in", values, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdNotIn(List<Long> values) {
            addCriterion("bar_goods_id not in", values, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdBetween(Long value1, Long value2) {
            addCriterion("bar_goods_id between", value1, value2, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBarGoodsIdNotBetween(Long value1, Long value2) {
            addCriterion("bar_goods_id not between", value1, value2, "barGoodsId");
            return (Criteria) this;
        }

        public Criteria andBatImgIsNull() {
            addCriterion("bat_img is null");
            return (Criteria) this;
        }

        public Criteria andBatImgIsNotNull() {
            addCriterion("bat_img is not null");
            return (Criteria) this;
        }

        public Criteria andBatImgEqualTo(String value) {
            addCriterion("bat_img =", value, "batImg");
            return (Criteria) this;
        }

        public Criteria andBatImgNotEqualTo(String value) {
            addCriterion("bat_img <>", value, "batImg");
            return (Criteria) this;
        }

        public Criteria andBatImgGreaterThan(String value) {
            addCriterion("bat_img >", value, "batImg");
            return (Criteria) this;
        }

        public Criteria andBatImgGreaterThanOrEqualTo(String value) {
            addCriterion("bat_img >=", value, "batImg");
            return (Criteria) this;
        }

        public Criteria andBatImgLessThan(String value) {
            addCriterion("bat_img <", value, "batImg");
            return (Criteria) this;
        }

        public Criteria andBatImgLessThanOrEqualTo(String value) {
            addCriterion("bat_img <=", value, "batImg");
            return (Criteria) this;
        }

        public Criteria andBatImgLike(String value) {
            addCriterion("bat_img like", value, "batImg");
            return (Criteria) this;
        }

        public Criteria andBatImgNotLike(String value) {
            addCriterion("bat_img not like", value, "batImg");
            return (Criteria) this;
        }

        public Criteria andBatImgIn(List<String> values) {
            addCriterion("bat_img in", values, "batImg");
            return (Criteria) this;
        }

        public Criteria andBatImgNotIn(List<String> values) {
            addCriterion("bat_img not in", values, "batImg");
            return (Criteria) this;
        }

        public Criteria andBatImgBetween(String value1, String value2) {
            addCriterion("bat_img between", value1, value2, "batImg");
            return (Criteria) this;
        }

        public Criteria andBatImgNotBetween(String value1, String value2) {
            addCriterion("bat_img not between", value1, value2, "batImg");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIsNull() {
            addCriterion("goods_id is null");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIsNotNull() {
            addCriterion("goods_id is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsIdEqualTo(Integer value) {
            addCriterion("goods_id =", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotEqualTo(Integer value) {
            addCriterion("goods_id <>", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdGreaterThan(Integer value) {
            addCriterion("goods_id >", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goods_id >=", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdLessThan(Integer value) {
            addCriterion("goods_id <", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdLessThanOrEqualTo(Integer value) {
            addCriterion("goods_id <=", value, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdIn(List<Integer> values) {
            addCriterion("goods_id in", values, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotIn(List<Integer> values) {
            addCriterion("goods_id not in", values, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdBetween(Integer value1, Integer value2) {
            addCriterion("goods_id between", value1, value2, "goodsId");
            return (Criteria) this;
        }

        public Criteria andGoodsIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goods_id not between", value1, value2, "goodsId");
            return (Criteria) this;
        }

        public Criteria andBarNameIsNull() {
            addCriterion("bar_name is null");
            return (Criteria) this;
        }

        public Criteria andBarNameIsNotNull() {
            addCriterion("bar_name is not null");
            return (Criteria) this;
        }

        public Criteria andBarNameEqualTo(String value) {
            addCriterion("bar_name =", value, "barName");
            return (Criteria) this;
        }

        public Criteria andBarNameNotEqualTo(String value) {
            addCriterion("bar_name <>", value, "barName");
            return (Criteria) this;
        }

        public Criteria andBarNameGreaterThan(String value) {
            addCriterion("bar_name >", value, "barName");
            return (Criteria) this;
        }

        public Criteria andBarNameGreaterThanOrEqualTo(String value) {
            addCriterion("bar_name >=", value, "barName");
            return (Criteria) this;
        }

        public Criteria andBarNameLessThan(String value) {
            addCriterion("bar_name <", value, "barName");
            return (Criteria) this;
        }

        public Criteria andBarNameLessThanOrEqualTo(String value) {
            addCriterion("bar_name <=", value, "barName");
            return (Criteria) this;
        }

        public Criteria andBarNameLike(String value) {
            addCriterion("bar_name like", value, "barName");
            return (Criteria) this;
        }

        public Criteria andBarNameNotLike(String value) {
            addCriterion("bar_name not like", value, "barName");
            return (Criteria) this;
        }

        public Criteria andBarNameIn(List<String> values) {
            addCriterion("bar_name in", values, "barName");
            return (Criteria) this;
        }

        public Criteria andBarNameNotIn(List<String> values) {
            addCriterion("bar_name not in", values, "barName");
            return (Criteria) this;
        }

        public Criteria andBarNameBetween(String value1, String value2) {
            addCriterion("bar_name between", value1, value2, "barName");
            return (Criteria) this;
        }

        public Criteria andBarNameNotBetween(String value1, String value2) {
            addCriterion("bar_name not between", value1, value2, "barName");
            return (Criteria) this;
        }

        public Criteria andBarPriceIsNull() {
            addCriterion("bar_price is null");
            return (Criteria) this;
        }

        public Criteria andBarPriceIsNotNull() {
            addCriterion("bar_price is not null");
            return (Criteria) this;
        }

        public Criteria andBarPriceEqualTo(BigDecimal value) {
            addCriterion("bar_price =", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceNotEqualTo(BigDecimal value) {
            addCriterion("bar_price <>", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceGreaterThan(BigDecimal value) {
            addCriterion("bar_price >", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("bar_price >=", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceLessThan(BigDecimal value) {
            addCriterion("bar_price <", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("bar_price <=", value, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceIn(List<BigDecimal> values) {
            addCriterion("bar_price in", values, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceNotIn(List<BigDecimal> values) {
            addCriterion("bar_price not in", values, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bar_price between", value1, value2, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bar_price not between", value1, value2, "barPrice");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinIsNull() {
            addCriterion("bar_money_min is null");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinIsNotNull() {
            addCriterion("bar_money_min is not null");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinEqualTo(BigDecimal value) {
            addCriterion("bar_money_min =", value, "barMoneyMin");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinNotEqualTo(BigDecimal value) {
            addCriterion("bar_money_min <>", value, "barMoneyMin");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinGreaterThan(BigDecimal value) {
            addCriterion("bar_money_min >", value, "barMoneyMin");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("bar_money_min >=", value, "barMoneyMin");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinLessThan(BigDecimal value) {
            addCriterion("bar_money_min <", value, "barMoneyMin");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinLessThanOrEqualTo(BigDecimal value) {
            addCriterion("bar_money_min <=", value, "barMoneyMin");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinIn(List<BigDecimal> values) {
            addCriterion("bar_money_min in", values, "barMoneyMin");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinNotIn(List<BigDecimal> values) {
            addCriterion("bar_money_min not in", values, "barMoneyMin");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bar_money_min between", value1, value2, "barMoneyMin");
            return (Criteria) this;
        }

        public Criteria andBarMoneyMinNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bar_money_min not between", value1, value2, "barMoneyMin");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumIsNull() {
            addCriterion("bar_people_num is null");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumIsNotNull() {
            addCriterion("bar_people_num is not null");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumEqualTo(Integer value) {
            addCriterion("bar_people_num =", value, "barPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumNotEqualTo(Integer value) {
            addCriterion("bar_people_num <>", value, "barPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumGreaterThan(Integer value) {
            addCriterion("bar_people_num >", value, "barPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("bar_people_num >=", value, "barPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumLessThan(Integer value) {
            addCriterion("bar_people_num <", value, "barPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumLessThanOrEqualTo(Integer value) {
            addCriterion("bar_people_num <=", value, "barPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumIn(List<Integer> values) {
            addCriterion("bar_people_num in", values, "barPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumNotIn(List<Integer> values) {
            addCriterion("bar_people_num not in", values, "barPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumBetween(Integer value1, Integer value2) {
            addCriterion("bar_people_num between", value1, value2, "barPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBarPeopleNumNotBetween(Integer value1, Integer value2) {
            addCriterion("bar_people_num not between", value1, value2, "barPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumIsNull() {
            addCriterion("bmbar_people_num is null");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumIsNotNull() {
            addCriterion("bmbar_people_num is not null");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumEqualTo(Integer value) {
            addCriterion("bmbar_people_num =", value, "bmbarPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumNotEqualTo(Integer value) {
            addCriterion("bmbar_people_num <>", value, "bmbarPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumGreaterThan(Integer value) {
            addCriterion("bmbar_people_num >", value, "bmbarPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("bmbar_people_num >=", value, "bmbarPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumLessThan(Integer value) {
            addCriterion("bmbar_people_num <", value, "bmbarPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumLessThanOrEqualTo(Integer value) {
            addCriterion("bmbar_people_num <=", value, "bmbarPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumIn(List<Integer> values) {
            addCriterion("bmbar_people_num in", values, "bmbarPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumNotIn(List<Integer> values) {
            addCriterion("bmbar_people_num not in", values, "bmbarPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumBetween(Integer value1, Integer value2) {
            addCriterion("bmbar_people_num between", value1, value2, "bmbarPeopleNum");
            return (Criteria) this;
        }

        public Criteria andBmbarPeopleNumNotBetween(Integer value1, Integer value2) {
            addCriterion("bmbar_people_num not between", value1, value2, "bmbarPeopleNum");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumIsNull() {
            addCriterion("seccess_people_num is null");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumIsNotNull() {
            addCriterion("seccess_people_num is not null");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumEqualTo(Integer value) {
            addCriterion("seccess_people_num =", value, "seccessPeopleNum");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumNotEqualTo(Integer value) {
            addCriterion("seccess_people_num <>", value, "seccessPeopleNum");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumGreaterThan(Integer value) {
            addCriterion("seccess_people_num >", value, "seccessPeopleNum");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("seccess_people_num >=", value, "seccessPeopleNum");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumLessThan(Integer value) {
            addCriterion("seccess_people_num <", value, "seccessPeopleNum");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumLessThanOrEqualTo(Integer value) {
            addCriterion("seccess_people_num <=", value, "seccessPeopleNum");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumIn(List<Integer> values) {
            addCriterion("seccess_people_num in", values, "seccessPeopleNum");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumNotIn(List<Integer> values) {
            addCriterion("seccess_people_num not in", values, "seccessPeopleNum");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumBetween(Integer value1, Integer value2) {
            addCriterion("seccess_people_num between", value1, value2, "seccessPeopleNum");
            return (Criteria) this;
        }

        public Criteria andSeccessPeopleNumNotBetween(Integer value1, Integer value2) {
            addCriterion("seccess_people_num not between", value1, value2, "seccessPeopleNum");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangIsNull() {
            addCriterion("goods_xianliang is null");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangIsNotNull() {
            addCriterion("goods_xianliang is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangEqualTo(Integer value) {
            addCriterion("goods_xianliang =", value, "goodsXianliang");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangNotEqualTo(Integer value) {
            addCriterion("goods_xianliang <>", value, "goodsXianliang");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangGreaterThan(Integer value) {
            addCriterion("goods_xianliang >", value, "goodsXianliang");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangGreaterThanOrEqualTo(Integer value) {
            addCriterion("goods_xianliang >=", value, "goodsXianliang");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangLessThan(Integer value) {
            addCriterion("goods_xianliang <", value, "goodsXianliang");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangLessThanOrEqualTo(Integer value) {
            addCriterion("goods_xianliang <=", value, "goodsXianliang");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangIn(List<Integer> values) {
            addCriterion("goods_xianliang in", values, "goodsXianliang");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangNotIn(List<Integer> values) {
            addCriterion("goods_xianliang not in", values, "goodsXianliang");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangBetween(Integer value1, Integer value2) {
            addCriterion("goods_xianliang between", value1, value2, "goodsXianliang");
            return (Criteria) this;
        }

        public Criteria andGoodsXianliangNotBetween(Integer value1, Integer value2) {
            addCriterion("goods_xianliang not between", value1, value2, "goodsXianliang");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuIsNull() {
            addCriterion("goods_shengyu is null");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuIsNotNull() {
            addCriterion("goods_shengyu is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuEqualTo(Integer value) {
            addCriterion("goods_shengyu =", value, "goodsShengyu");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuNotEqualTo(Integer value) {
            addCriterion("goods_shengyu <>", value, "goodsShengyu");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuGreaterThan(Integer value) {
            addCriterion("goods_shengyu >", value, "goodsShengyu");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuGreaterThanOrEqualTo(Integer value) {
            addCriterion("goods_shengyu >=", value, "goodsShengyu");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuLessThan(Integer value) {
            addCriterion("goods_shengyu <", value, "goodsShengyu");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuLessThanOrEqualTo(Integer value) {
            addCriterion("goods_shengyu <=", value, "goodsShengyu");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuIn(List<Integer> values) {
            addCriterion("goods_shengyu in", values, "goodsShengyu");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuNotIn(List<Integer> values) {
            addCriterion("goods_shengyu not in", values, "goodsShengyu");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuBetween(Integer value1, Integer value2) {
            addCriterion("goods_shengyu between", value1, value2, "goodsShengyu");
            return (Criteria) this;
        }

        public Criteria andGoodsShengyuNotBetween(Integer value1, Integer value2) {
            addCriterion("goods_shengyu not between", value1, value2, "goodsShengyu");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("start_time is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("start_time is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Date value) {
            addCriterion("start_time =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Date value) {
            addCriterion("start_time <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Date value) {
            addCriterion("start_time >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("start_time >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Date value) {
            addCriterion("start_time <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("start_time <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Date> values) {
            addCriterion("start_time in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Date> values) {
            addCriterion("start_time not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Date value1, Date value2) {
            addCriterion("start_time between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("start_time not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("end_time is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("end_time is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Date value) {
            addCriterion("end_time =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Date value) {
            addCriterion("end_time <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Date value) {
            addCriterion("end_time >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("end_time >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Date value) {
            addCriterion("end_time <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("end_time <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Date> values) {
            addCriterion("end_time in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Date> values) {
            addCriterion("end_time not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Date value1, Date value2) {
            addCriterion("end_time between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("end_time not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andBarStatusIsNull() {
            addCriterion("bar_status is null");
            return (Criteria) this;
        }

        public Criteria andBarStatusIsNotNull() {
            addCriterion("bar_status is not null");
            return (Criteria) this;
        }

        public Criteria andBarStatusEqualTo(String value) {
            addCriterion("bar_status =", value, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarStatusNotEqualTo(String value) {
            addCriterion("bar_status <>", value, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarStatusGreaterThan(String value) {
            addCriterion("bar_status >", value, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarStatusGreaterThanOrEqualTo(String value) {
            addCriterion("bar_status >=", value, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarStatusLessThan(String value) {
            addCriterion("bar_status <", value, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarStatusLessThanOrEqualTo(String value) {
            addCriterion("bar_status <=", value, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarStatusLike(String value) {
            addCriterion("bar_status like", value, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarStatusNotLike(String value) {
            addCriterion("bar_status not like", value, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarStatusIn(List<String> values) {
            addCriterion("bar_status in", values, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarStatusNotIn(List<String> values) {
            addCriterion("bar_status not in", values, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarStatusBetween(String value1, String value2) {
            addCriterion("bar_status between", value1, value2, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarStatusNotBetween(String value1, String value2) {
            addCriterion("bar_status not between", value1, value2, "barStatus");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeIsNull() {
            addCriterion("bar_youxiao_time is null");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeIsNotNull() {
            addCriterion("bar_youxiao_time is not null");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeEqualTo(Integer value) {
            addCriterion("bar_youxiao_time =", value, "barYouxiaoTime");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeNotEqualTo(Integer value) {
            addCriterion("bar_youxiao_time <>", value, "barYouxiaoTime");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeGreaterThan(Integer value) {
            addCriterion("bar_youxiao_time >", value, "barYouxiaoTime");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("bar_youxiao_time >=", value, "barYouxiaoTime");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeLessThan(Integer value) {
            addCriterion("bar_youxiao_time <", value, "barYouxiaoTime");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeLessThanOrEqualTo(Integer value) {
            addCriterion("bar_youxiao_time <=", value, "barYouxiaoTime");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeIn(List<Integer> values) {
            addCriterion("bar_youxiao_time in", values, "barYouxiaoTime");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeNotIn(List<Integer> values) {
            addCriterion("bar_youxiao_time not in", values, "barYouxiaoTime");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeBetween(Integer value1, Integer value2) {
            addCriterion("bar_youxiao_time between", value1, value2, "barYouxiaoTime");
            return (Criteria) this;
        }

        public Criteria andBarYouxiaoTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("bar_youxiao_time not between", value1, value2, "barYouxiaoTime");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentIsNull() {
            addCriterion("bar_goods_content is null");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentIsNotNull() {
            addCriterion("bar_goods_content is not null");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentEqualTo(String value) {
            addCriterion("bar_goods_content =", value, "barGoodsContent");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentNotEqualTo(String value) {
            addCriterion("bar_goods_content <>", value, "barGoodsContent");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentGreaterThan(String value) {
            addCriterion("bar_goods_content >", value, "barGoodsContent");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentGreaterThanOrEqualTo(String value) {
            addCriterion("bar_goods_content >=", value, "barGoodsContent");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentLessThan(String value) {
            addCriterion("bar_goods_content <", value, "barGoodsContent");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentLessThanOrEqualTo(String value) {
            addCriterion("bar_goods_content <=", value, "barGoodsContent");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentLike(String value) {
            addCriterion("bar_goods_content like", value, "barGoodsContent");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentNotLike(String value) {
            addCriterion("bar_goods_content not like", value, "barGoodsContent");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentIn(List<String> values) {
            addCriterion("bar_goods_content in", values, "barGoodsContent");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentNotIn(List<String> values) {
            addCriterion("bar_goods_content not in", values, "barGoodsContent");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentBetween(String value1, String value2) {
            addCriterion("bar_goods_content between", value1, value2, "barGoodsContent");
            return (Criteria) this;
        }

        public Criteria andBarGoodsContentNotBetween(String value1, String value2) {
            addCriterion("bar_goods_content not between", value1, value2, "barGoodsContent");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}