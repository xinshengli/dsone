package com.qianxi.user.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.pojo.TbAddress;
import com.qianxi.user.service.TbAddressService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbAddress")
@RestController
public class TbAddressController {

    @Reference
    private TbAddressService tbAddressService;

    @RequestMapping("findAll")
    public List<TbAddress> findAll() {
        return tbAddressService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbAddress
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbAddress> pageList(@RequestBody(required = false) TbAddress tbAddress,
                                        @RequestParam(defaultValue = "3") int pageSize,
                                        @RequestParam(defaultValue = "1") int pageNum){

        return tbAddressService.pageList(tbAddress,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbAddress
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbAddress tbAddress){
        try {
            tbAddressService.save(tbAddress);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbAddressService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbAddress getById(Long id){
        return tbAddressService.getById(id);
    }

}
