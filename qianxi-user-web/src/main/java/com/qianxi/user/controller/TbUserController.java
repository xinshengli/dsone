package com.qianxi.user.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import com.qianxi.pojo.TbUser;
import com.qianxi.user.service.TbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbUser")
@RestController
public class TbUserController {

    @Reference
    private TbUserService tbUserService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    @RequestMapping("redisnx")
    public void reidssetnx(){
        Boolean result = redisTemplate.opsForValue().setIfAbsent("520","520");
/*        System.out.println("没有set值的情况下是："+result);
        redisTemplate.opsForValue().set("520","520");
        Boolean result2 = redisTemplate.opsForValue().setIfAbsent("520","520");
        System.out.println("set值的情况下是："+result2);*/
        if(result){
            System.out.println("这是我要执行的业务");
            System.out.println("这是我要执行的业务");
            System.out.println("这是我要执行的业务");
            System.out.println("这是我要执行的业务");
           redisTemplate.opsForValue().set("520","520");
        }
    }


    @RequestMapping("findAll")
    public List<TbUser> findAll() {
        return tbUserService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbUser
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbUser> pageList(@RequestBody(required = false) TbUser tbUser,
                                        @RequestParam(defaultValue = "3") int pageSize,
                                        @RequestParam(defaultValue = "1") int pageNum){

        return tbUserService.pageList(tbUser,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbUser
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbUser tbUser){
        try {
            tbUserService.save(tbUser);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbUserService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbUser getById(Long id){
        return tbUserService.getById(id);
    }

}
