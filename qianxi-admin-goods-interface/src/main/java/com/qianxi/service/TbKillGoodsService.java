package com.qianxi.service;

import com.qianxi.pojo.TbKillGoods;
import com.qianxi.entity.PageResult;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbKillGoodsService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbKillGoods> findAll();

    PageResult<TbKillGoods> pageList(TbKillGoods tbKillGoods, int pageSize, int pageNum);

    void save(TbKillGoods tbKillGoods);

    void deletebth(Long[] ids);

    TbKillGoods getById(Long id);
}
