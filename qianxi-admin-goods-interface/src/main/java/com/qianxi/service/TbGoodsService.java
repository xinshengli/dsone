package com.qianxi.service;

import com.qianxi.entity.PageResult;
import com.qianxi.pojo.TbGoods;

import java.util.List;

public interface TbGoodsService {
    public List<TbGoods> findAll();

    PageResult<TbGoods> pageList(TbGoods tbGoods, Integer pageSize, Integer pageNum);

    void saveTbGoods(TbGoods tbGoods);

    void deleteTbGoods(Long[] ids);

    TbGoods getById(Long id);
}
