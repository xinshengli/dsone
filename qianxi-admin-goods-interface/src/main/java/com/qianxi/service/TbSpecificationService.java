package com.qianxi.service;

import com.qianxi.pojo.TbSpecification;
import com.qianxi.entity.PageResult;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbSpecificationService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbSpecification> findAll();

    PageResult<TbSpecification> pageList(TbSpecification tbSpecification, int pageSize, int pageNum);

    void save(TbSpecification tbSpecification);

    void deletebth(Long[] ids);

    TbSpecification getById(Long id);
}
