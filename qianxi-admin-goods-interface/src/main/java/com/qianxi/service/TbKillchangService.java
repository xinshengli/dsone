package com.qianxi.service;

import com.qianxi.entity.PageResult;
import com.qianxi.pojo.TbKillChang;

import java.util.List;

public interface TbKillchangService {

    public List<TbKillChang> findAll();

    PageResult<TbKillChang> pageList(TbKillChang tbKillchang, Integer pageSize, Integer pageNum);

    void saveKillChang(TbKillChang tbKillchang);

    void deleteKillChang(Long[] ids);

    TbKillChang getById(Long id);
}
