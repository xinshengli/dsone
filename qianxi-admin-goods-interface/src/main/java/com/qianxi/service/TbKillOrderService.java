package com.qianxi.service;

import com.qianxi.pojo.TbKillOrder;
import com.qianxi.entity.PageResult;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbKillOrderService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbKillOrder> findAll();

    PageResult<TbKillOrder> pageList(TbKillOrder tbKillOrder, int pageSize, int pageNum);

    void save(TbKillOrder tbKillOrder);

    void deletebth(Long[] ids);

    TbKillOrder getById(Long id);
}
