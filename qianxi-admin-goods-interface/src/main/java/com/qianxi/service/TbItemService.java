package com.qianxi.service;

import com.qianxi.entity.PageResult;
import com.qianxi.pojo.TbItem;

import java.util.List;

public interface TbItemService {
    public List<TbItem> findAll();

    PageResult<TbItem> pageList(TbItem item, Integer pageSize, Integer pageNum);

    void saveTbItem(TbItem item);

    void deleteTbItem(Long[] ids);

    TbItem getById(Long id);

    List<TbItem> getByGoodsId(Long id);
}
