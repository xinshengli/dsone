package com.qianxi.service;

import com.qianxi.pojo.TbGoodsDesc;
import com.qianxi.entity.PageResult;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbGoodsDescService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbGoodsDesc> findAll();

    PageResult<TbGoodsDesc> pageList(TbGoodsDesc tbGoodsDesc, int pageSize, int pageNum);

    void save(TbGoodsDesc tbGoodsDesc);

    void deletebth(Long[] ids);

    TbGoodsDesc getById(Long id);
}
