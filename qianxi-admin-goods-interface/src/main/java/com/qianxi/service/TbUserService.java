package com.qianxi.service;

import com.qianxi.pojo.TbUser;
import com.qianxi.entity.PageResult;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbUserService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbUser> findAll();

    PageResult<TbUser> pageList(TbUser tbUser, int pageSize, int pageNum);

    void save(TbUser tbUser);

    void deletebth(Long[] ids);

    TbUser getById(Long id);
}
