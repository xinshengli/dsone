package com.qianxi.service;

import com.qianxi.pojo.TbDistrict;
import com.qianxi.entity.PageResult;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbDistrictService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbDistrict> findAll();

    PageResult<TbDistrict> pageList(TbDistrict tbDistrict, int pageSize, int pageNum);

    void save(TbDistrict tbDistrict);

    void deletebth(String[] ids);

    TbDistrict getById(String id);
}
