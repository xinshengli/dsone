package com.qianxi.service;

import com.qianxi.pojo.TbPayLog;
import com.qianxi.entity.PageResult;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbPayLogService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbPayLog> findAll();

    PageResult<TbPayLog> pageList(TbPayLog tbPayLog, int pageSize, int pageNum);

    void save(TbPayLog tbPayLog);

    void deletebth(String[] ids);

    TbPayLog getById(String id);
}
