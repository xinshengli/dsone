package com.qianxi.service;

import com.qianxi.pojo.TbSpecificationOption;
import com.qianxi.entity.PageResult;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbSpecificationOptionService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbSpecificationOption> findAll();

    PageResult<TbSpecificationOption> pageList(TbSpecificationOption tbSpecificationOption, int pageSize, int pageNum);

    void save(TbSpecificationOption tbSpecificationOption);

    void deletebth(Long[] ids);

    TbSpecificationOption getById(Long id);
}
