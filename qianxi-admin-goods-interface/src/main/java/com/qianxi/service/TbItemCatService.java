package com.qianxi.service;

import com.qianxi.pojo.TbItemCat;
import com.qianxi.entity.PageResult;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbItemCatService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbItemCat> findAll();

    PageResult<TbItemCat> pageList(TbItemCat tbItemCat, int pageSize, int pageNum);

    void save(TbItemCat tbItemCat);

    void deletebth(Long[] ids);

    TbItemCat getById(Long id);
}
