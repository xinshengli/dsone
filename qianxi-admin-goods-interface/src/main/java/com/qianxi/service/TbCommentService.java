package com.qianxi.service;

import com.qianxi.pojo.TbComment;
import com.qianxi.entity.PageResult;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbCommentService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbComment> findAll();

    PageResult<TbComment> pageList(TbComment tbComment, int pageSize, int pageNum);

    void save(TbComment tbComment);

    void deletebth(Long[] ids);

    TbComment getById(Long id);
}
