package com.qianxi.bargain.service;

import com.qianxi.entity.PageResult;
import com.qianxi.pojo.TbBargainRecord;


import java.util.List;

public interface TbBargainRecordService {


    /**
     * 返回全部列表
     * @return
     */
    public List<TbBargainRecord> findAll();

    PageResult<TbBargainRecord> pageList(TbBargainRecord tbBargainRecord, int pageSize, int pageNum);

    void save(TbBargainRecord tbBargainRecord);

    void deletebth(Long[] ids);

    TbBargainRecord getById(Long id);
    TbBargainRecord  getkanjia(Long barId);
}



