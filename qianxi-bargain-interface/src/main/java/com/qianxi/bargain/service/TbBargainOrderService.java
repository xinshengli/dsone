package com.qianxi.bargain.service;

import com.qianxi.entity.PageResult;
import com.qianxi.pojo.TbBargainOrder;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbBargainOrderService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbBargainOrder> findAll();

    PageResult<TbBargainOrder> pageList(TbBargainOrder tbBargainOrder, int pageSize, int pageNum);

    void save(TbBargainOrder tbBargainOrder);

    void deletebth(Long[] ids);

    TbBargainOrder getById(Long id);
}
