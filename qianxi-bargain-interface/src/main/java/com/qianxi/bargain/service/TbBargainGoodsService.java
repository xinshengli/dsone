package com.qianxi.bargain.service;

import com.qianxi.entity.PageResult;
import com.qianxi.pojo.TbBargainGoods;

import java.util.List;

public interface TbBargainGoodsService {
    public List<TbBargainGoods> findAll();

    PageResult<TbBargainGoods> pageList(TbBargainGoods tbBargainGoods, int pageSize, int pageNum);

    void save(TbBargainGoods tbBargainGoods);

    void deletebth(Long[] ids);

    TbBargainGoods getById(Long id);
}
