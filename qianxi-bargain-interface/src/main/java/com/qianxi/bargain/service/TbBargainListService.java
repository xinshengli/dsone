package com.qianxi.bargain.service;

import com.qianxi.entity.PageResult;
import com.qianxi.pojo.TbBargainList;


import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbBargainListService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbBargainList> findAll();

    PageResult<TbBargainList> pageList(TbBargainList tbBargainList, int pageSize, int pageNum);

    void save(TbBargainList tbBargainList);

    void deletebth(Long[] ids);

    TbBargainList getById(Long id);
}
