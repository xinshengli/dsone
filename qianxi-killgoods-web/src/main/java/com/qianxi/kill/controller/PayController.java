package com.qianxi.kill.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.qianxi.pojo.TbKillGoods;
import com.qianxi.pojo.TbKillOrder;
import com.qianxi.service.TbKillGoodsService;
import com.qianxi.service.TbKillchangService;
import com.qianxi.user.service.TbAddressService;
import com.qianxi.utils.IdWorker;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2021/1/1515:44
 */
@RequestMapping("seckillpay")
@RestController
public class PayController {
    @Autowired
    private RedisTemplate redisTemplate;

    @Reference
    private TbKillGoodsService tbKillGoodsService;

    @Reference
    private TbKillchangService tbKillchangService;

    @Reference
    private TbAddressService tbAddressService;

    @Resource
    private IdWorker idWorker;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 生成二维码
     * @return
     */
    @RequestMapping("/createNative")
    public Map createNative(){
        //获取当前用户

        String userId= SecurityContextHolder.getContext().getAuthentication().getName();
      /*  //到redis查询秒杀订单
        TbSeckillOrder seckillOrder=(TbSeckillOrder)redisTemplate.boundHashOps("seckillOrder").get(userId);
        //判断秒杀订单存在
        if(seckillOrder!=null){
            return weixinPayService.createNative(seckillOrder.getId()+"",seckillOrder.getMoney());
        }else{
            return new HashMap();
        }*/
      return null;
    }

    @RequestMapping("alipaySum")
    public Object alipayIumpSum(Model model, String payables, String subject, String body, HttpServletResponse response)
            throws Exception {
        // 获得初始化的AlipayClient

        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id,
                AlipayConfig.merchant_private_key, "json", AlipayConfig.charset,
                AlipayConfig.alipay_public_key, AlipayConfig.sign_type);
        // 设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayConfig.return_url);
        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        // 商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = sdf.format(new Date());
        // 付款金额，必填
        String total_amount = payables.replace(",", "5000");
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\"," + "\"total_amount\":\"" + total_amount
                + "\"," + "\"subject\":\"" + subject + "\"," + "\"body\":\"" + body + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        // 请求
        String result = alipayClient.pageExecute(alipayRequest).getBody();
        // System.out.println(result);
        AlipayConfig.logResult(result);// 记录支付日志
        response.setContentType("text/html; charset=gbk");
        PrintWriter out = response.getWriter();
        out.print(result);
        return "alipayexit";
    }

    /**
     * p2p后台返回的操作
     * @param response，request
     * @throws Exception
     * @return void
     * @author AAA_有梦想一起实现
     * @date 2017年11月30日
     */
    @RequestMapping("notify_url")
    public void Notify(HttpServletResponse response, HttpServletRequest request) throws Exception {
        System.out.println("----------------------------notify_url------------------------");
        // 商户订单号
        String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "GBK");
        // 付款金额
        String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"), "GBK");
        // 支付宝交易号
        String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "GBK");
        // 交易说明
        String cus = new String(request.getParameter("body").getBytes("ISO-8859-1"), "GBK");
        // 交易状态
        String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "GBK");
        if (trade_status.equals("TRADE_SUCCESS")) {//支付成功商家操作
            //下面是我写的一个简单的插入操作，根据你的操作自行编写
            /*Map<Object, Object> map = new HashMap<Object, Object>();
            map.put("cuId", Integer.valueOf(cus));
            RepaymentPlan repaymentPlan = new RepaymentPlan();
            Integer id = Integer.valueOf(out_trade_no);
            double payablesCheck = Double.valueOf(total_amount);
            RepaymentPlan repayCheck = serviceMain.selectByPrimaryKey(id);
            double total = repayCheck.getPayables();
            if (Double.valueOf(total_amount) < repayCheck.getPayables()) {
                map.put("ubalance", total - Double.valueOf(total_amount));
                serviceMain.updateCusMoney(map);
            }
            repaymentPlan.setId(id);
            repaymentPlan.setActualPayment(total);
            repaymentPlan.setRepaymentStatus(1);
            int i = serviceMain.updateByPrimaryKeySelective(repaymentPlan);
            System.out.println("---------------------还款影响行数----------------------------" + i);*/
        }
    }


    /**
     * 同步通知的页面的Controller
     *
     * @param
     * @throws InterruptedException
     */
    @RequestMapping("return_url")
    public String Return_url() throws InterruptedException {
        return "alipayexit";
    }




    @RequestMapping("paySuccess")
    public String paySuccess(){

        String user_id = SecurityContextHolder.getContext().getAuthentication().getName();
        TbKillOrder tbKillOrder = (TbKillOrder) redisTemplate.boundHashOps("killOrder").get(user_id);
        if(tbKillOrder!=null){
            //支付成功后要修改reids中的订单状态
            tbKillOrder.setStatus("1");
            redisTemplate.opsForHash().put("killOrder",user_id,tbKillOrder);
        }

        TbKillOrder tbKillOrder2 = (TbKillOrder) redisTemplate.opsForHash().get("killOrder", user_id);
        System.out.println("支付成功修改后的订单"+tbKillOrder2);



        String s2  = JSON.toJSONString(tbKillOrder2);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("killgoods", "");
        map.put("killOrder", s2);
        //根据key发送到对应的队列

        //订单转为jsog格式字符串发送到我们的消息队列中
        rabbitTemplate.convertAndSend("queuekey", map);
        return "true";
//        Boolean aBoolean = redisTemplate.opsForValue().setIfAbsent("22222222", "3333");
    }
}
