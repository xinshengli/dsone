package com.qianxi.kill.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qianxi.pojo.TbKillGoods;
import com.qianxi.pojo.TbKillOrder;
import com.qianxi.service.TbKillGoodsService;
import com.qianxi.service.TbKillchangService;
import com.qianxi.service.TbKillorderService;
import com.qianxi.user.service.TbAddressService;
import com.qianxi.utils.IdWorker;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

public class RecvHandler implements MessageListener {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    @Autowired
    private RedisTemplate redisTemplate;

    @Reference
    private TbKillGoodsService tbKillGoodsService;

    @Reference
    private TbKillchangService tbKillchangService;

    @Reference
    private TbAddressService tbAddressService;

    @Reference
    private TbKillorderService tbKillorderService;

    @Resource
    private IdWorker idWorker;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void onMessage(Message msg) {
        try {
            // msg就是rabbitmq传来的消息，需要的同学自己打印看一眼
            // 使用jackson解析
            JsonNode jsonData = MAPPER.readTree(msg.getBody());
            String killgoods = jsonData.get("killgoods").asText();
            //如果监听段收到的字符串不为空
            if (killgoods != "") {
                List<TbKillGoods> tbKillGoodsList = JSON.parseArray(killgoods, TbKillGoods.class);
                for (TbKillGoods tbKillGoods : tbKillGoodsList) {
                    redisTemplate.opsForHash().put("goodsStock", tbKillGoods.getItemId() + "", tbKillGoods.getStock());
                }

                Integer i1 = (Integer) redisTemplate.opsForHash().get("goodsStock", 917460 + "");
                Integer i2 = (Integer) redisTemplate.opsForHash().get("goodsStock", 917461 + "");
                Integer i3 = (Integer) redisTemplate.opsForHash().get("goodsStock", 917770 + "");
                Integer i4 = (Integer) redisTemplate.opsForHash().get("goodsStock", 919669 + "");
                Integer i5 = (Integer) redisTemplate.opsForHash().get("goodsStock", 925237 + "");
                System.out.println(i1);
                System.out.println(i2);
                System.out.println(i3);
                System.out.println(i4);
                System.out.println(i5);
            }
            String killOrder = jsonData.get("killOrder").asText();
            if (killOrder != "") {
                TbKillOrder tbKillOrder = JSON.parseObject(killOrder, TbKillOrder.class);
                System.out.println("mq需要处理的订单为：" + tbKillOrder);
                Boolean aBoolean = redisTemplate.opsForValue().setIfAbsent(tbKillOrder.getId() + "", tbKillOrder.getId() + "");
                if (aBoolean) {
                    tbKillorderService.saveTbKillOrder(tbKillOrder);
                    Long itemId = tbKillOrder.getItemId();
                    TbKillGoods tbKillGoods = tbKillGoodsService.getById(itemId);
                    tbKillGoods.setStock(tbKillGoods.getStock() - 1);
                    tbKillGoodsService.updateByPrimaryKey(tbKillGoods);
                    redisTemplate.opsForValue().setIfAbsent(tbKillOrder.getId() + "", tbKillOrder.getId() + "");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
