package com.qianxi.kill.controller;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {

// ↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000117615426";//例：2016082600317257


    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key ="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCkheBbsRQ4A078kLX3FHME0JqKPbbILC8ojXqiYqdCKBKncRuoGmKx5Kuwog2TkjwbtPYUO5wJ3yrOPEwM8fUb6S8cudvpn0YIgo/mgb1XTZrW65YuddL78cuvNKqKUCnmO3SdCe6DgC6+O5U4qZStQe4E6NiGBnpEEf9rhFlid21fv4m9JfkBTAPEiVxVfqOmAzpKTAgZX75cfLNAAR+0BFocrmyjYfThjW7w+UUzU577TzjgPiyWJEaZS5BYIq8VHwIvJEdK7IJfVF26HRkWQKoYfgFBT101WPB3FYKjHKyX9fz86miC08ZV9LPFBz2HV1+oRT8cfvdjD21fNKBvAgMBAAECggEABPm/hUZftCn1c5H0ztoheF3OrqubIYakjndo5AxTvp5zofi7u2PA5FxkDN15x6K+ip/4aTg8Sdgj9UTBkAqpPQTaVeYRDomwloVMUM5PqZpBOoq5agJKNTBk1pGjLrep7LqTlkQ8oGF6CGrI4oPOaUb7Kc7Y7YS4oeJhdSD1ixLBAzAu01ML3MiOWYvkKHoxGjrT9km7EFnTH8w85dJS0C3/OVtgDouFr+mTVVZjfCtD3qatyKfx+GHXq+JnKAXtvK0SFIRzQx0RpePFU0i6Xegdtl7gNNoIkdr4PC0wEn1L4DjgbsuSZHElUozqJzgeRUYqESnPijy3TaHf/z0LIQKBgQDtCTGMjdeU+/e0N4DuCbPo/Gpu6JeaaOiEjtk4waGfwV8b0FMopruK5je7VKEFcg8sbNnvpReYVNqIoIk/C5zMw5VE2gZUt59GEvb/s5bI8hGW2PF9M4sujLf9mpCMR7uIbq4xpUVdOmkIdRgxMWlTCX2KA7Q4dVcG5Bhu7tpyqQKBgQCxr4W4iLQEdmvuJjBAREFaxPz/75Ca4rlsvfaRvDaaO2NykDfzZlv8hmioIGJ5zlhVYGfMB4VNsKZ5SuWS28DQKFwtPYp49fBmBerFvGDmqaztum+CR9vDuf73nxzTRDY2TmHM70fkymn/RhgXnOGpuwSLEuMVHOtK3VwL/H0BVwKBgAob6UQQTJBrCo5iC5qbSVP3Z+ag/s40CS6WFQeJooX4rkC2asWCDt+Gn/vaSR+9VI5VehnxVYdGnydSiWfp698DvFr6XWjBiAZqCqSvGRAq2rsvN2VajGYI1FP5go6pBIN2DPHLFaE7O32D2OtIKzDCZ9s5+zapMnP1yGD6yNA5AoGBAJo5s2N8pGPLNds7ol3l4KQcn0y2b1lAuZk9OSs9plcwfR0POME3c1g4gdaMZW08QUe5qd5aGMZaMeTNsX5K54zFu1w5MlvTfJnRQPnojGMpm0OssuTt4j58zmW34Zkud6+jURhECK75Ug+Uk4zLSsO75DGibyAAPep5QE/XE9l5AoGBAKzSkkn/8QcHvkJsjr0HgW7rEvUjkDTRtdTEr9iin+eHqKB5tXSVwQDIH/jPFPAKI6Qzr7ue87WPPEsSrOMyplIjj+NSs2a49VryK88mr5XCHEaUMjHb5Hy9p0pKypl4m9Wsxcesokf0GJs4aDuetAg8Cquj/Ll40k5eYLs4US7K";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm
    // 对应APPID下的支付宝公钥。
    public static String alipay_public_key ="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmFHz3cjCrpvB272LCX8ynN854RBqLYo5ewCx4Sxbj28jBW5v5ZUDvN61Z3lTOnkARVIqE+t0ttqsWhlfR8scj9idfdmnByCvJRo2cZZRGJuhoTHFY9rszsPQKqc6HEbDVTr1MRLgYsrdhqVZimVlFW9hWsFac8pCmePibeO2uZp6ejSj7FoDGboZYWeVkuzuF8jbFBPActsYiwDqvI/dqeFhH6c1/xFK5eh8cB9hTYvzbuxT6LJCejWqcqKkoAmCWhZwGXSXCgI7yuvsAV5Qg8EmQIq7msAct0c2sCXH62sUL6AjRKS6z4TvwXPDPtSQc+IHNi9KrQeut6fVr+wxTQIDAQAB";
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost:8087/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://localhost:8087/alipay.trade.page.pay-JAVA-UTF-8/return_url.jsp";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";


    // 支付宝网关
    public static String log_path = "D:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
