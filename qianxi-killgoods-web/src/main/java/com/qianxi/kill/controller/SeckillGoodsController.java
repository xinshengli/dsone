package com.qianxi.kill.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.qianxi.entity.Result;
import com.qianxi.pojo.TbAddress;
import com.qianxi.pojo.TbKillChang;
import com.qianxi.pojo.TbKillGoods;
import com.qianxi.pojo.TbKillOrder;
import com.qianxi.service.TbKillGoodsService;
import com.qianxi.service.TbKillchangService;
import com.qianxi.user.service.TbAddressService;
import com.qianxi.utils.DateUtils;
import com.qianxi.utils.IdWorker;
import com.qianxi.utils.SecurityUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2021/1/1513:56
 */

@RestController
@RequestMapping("seckill")
public class SeckillGoodsController {

    @Autowired
    private RedisTemplate redisTemplate;

    @Reference
    private TbKillGoodsService tbKillGoodsService;

    @Reference
    private TbKillchangService tbKillchangService;

    @Reference
    private TbAddressService tbAddressService;

    @Resource
    private IdWorker idWorker;

    @Autowired
    private RabbitTemplate rabbitTemplate;



    /**
     * 当前秒杀的商品
     *
     * @return
     */
    @RequestMapping("/findList")
    public List<TbKillGoods> findList() {
        //首先从缓存中获取秒杀商品列表
        List<TbKillGoods> tbKillGoodsList = redisTemplate.boundHashOps("killGoods").values();
        if (tbKillGoodsList == null || tbKillGoodsList.size() == 0) {
            System.out.println("=====================");
            tbKillGoodsList = tbKillGoodsService.findList();
            for (TbKillGoods tbKillGoods : tbKillGoodsList) {
                redisTemplate.boundHashOps("killGoods").put(tbKillGoods.getItemId() + "", tbKillGoods);
            }
        }
        return tbKillGoodsList;
    }

    //根据秒杀商品id获取redis中秒杀商品列表中对应的商品
    @RequestMapping("/findOneFromRedis")
    public TbKillGoods findOneFromRedis(Long seckillId) {
        return (TbKillGoods) redisTemplate.boundHashOps("killGoods").get(seckillId + "");
    }


    //开始秒杀
    @RequestMapping("/submitOrder")
    public Result submitOrder(Long seckillId) {

        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        String username = SecurityUtils.getLoginUsername();

        if ("anonymousUser".equals(userId)) {//如果未登录
            return new Result(false, 200, "用户未登录");
        }
        //从缓存中查询秒杀商品
        TbKillGoods tbKillGoods = (TbKillGoods) redisTemplate.boundHashOps("killGoods").get(seckillId + "");


        /**
         * 此代码块验证秒杀场次是否是正在进行的
         */
        //获取秒杀商品对象中的秒杀场次id
        Long killChangId = tbKillGoods.getKillChangId();

        //根据秒杀场次id获取秒杀场次对象
        TbKillChang tbKillChang = tbKillchangService.getById(killChangId);
        //开始时间
        String startTime = tbKillChang.getStartTime();
        //结束时间
        String endTime = tbKillChang.getEndTime();
        //当前时间
        String currentTime = DateUtils.getCurrentTime();
        //调用工具栏比较日期大小
        int i1 = DateUtils.compareDate(currentTime, startTime, "yyyy-MM-dd HH:mm:ss");
        int i2 = DateUtils.compareDate(endTime, currentTime, "yyyy-MM-dd HH:mm:ss");
        //如果当前时间大于开始时间小于结束时间
        if (i1 > 0 && i2 > 0) {
            System.out.println("可以秒杀！！");
        } else {
            System.out.println("不可以秒杀！！");
            return new Result(false, 200, "该商品还未进行秒杀");
        }

        /**
         * 验证此用户是否已经买过该商品
         *
         */
/*            List<Long> list = new ArrayList<>();
            list.add((long) 88888);
            redisTemplate.boundHashOps("killjilu").put(userId,list);*/
        //获取秒杀记录表
        List<Long> goodslist = (List<Long>) redisTemplate.boundHashOps("killjilu").get(userId);

        if (goodslist != null && goodslist.size() > 0) {
            for (Long s : goodslist) {
                System.out.println("秒杀过的商品id：" + s);
                if (s.equals(seckillId)) {
                    //如果id存在则返回不能购买
                    System.out.println("您已购买过该商品暂不能购买");
                    return new Result(false, 200, "您已购买过该商品暂不能购买！");
                }
            }
        }


        /**
         * 生成一个10以内的随机数，随机到3才能秒杀
         */
        int iRandom = (int) (1 + Math.random() * 10);
        iRandom = 3;
        if (iRandom != 3) {
            return new Result(false, 200, "您暂时不能购买该商品，请稍后再试！");
        }


        /**
         * 此代码块判断商品是否存在   存在的话是库存是否充足
         */
        if (tbKillGoods == null) {
            return new Result(false, 200, "商品不存在");
        }
        if (tbKillGoods.getStock() <= 0) {
            return new Result(false, 200, "商品已抢购一空");
        }


        //验证都通过扣减（redis）库存
        tbKillGoods.setStock(tbKillGoods.getStock() - 1);

        redisTemplate.boundHashOps("killGoods").put(seckillId + "", tbKillGoods);//扣减库存后的商品放回缓存
        redisTemplate.boundHashOps("goodsStock").put(seckillId + "", tbKillGoods.getStock());//扣减库存后的商品放回缓存



        //ddddddddddddddd
        if (goodslist == null) {
            goodslist = new ArrayList<>();
        }

        goodslist.add(seckillId);

        redisTemplate.boundHashOps("killjilu").put(userId, goodslist);


        if (tbKillGoods.getStock() == 0) {//如果已经被秒光
            tbKillGoodsService.updateByPrimaryKey(tbKillGoods);//同步到数据库
            redisTemplate.boundHashOps("seckillGoods").delete(seckillId + "");
        }


        //生成订单对象兵保存（redis）
        long orderId = idWorker.nextId();
        TbKillOrder tbKillOrder = new TbKillOrder();
        tbKillOrder.setId(orderId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        tbKillOrder.setCreateTime(sdf.format(new Date()));
        tbKillOrder.setMoney(tbKillGoods.getKillPrice());//秒杀价格
        tbKillOrder.setItemId(seckillId);
        tbKillOrder.setUserId(userId);//设置用户ID
        tbKillOrder.setStatus("0");//状态
        TbAddress defaultAddressByUserId = tbAddressService.getDefaultAddressByUserId(userId);
        tbKillOrder.setReceiverMobile(defaultAddressByUserId.getMobile());
        tbKillOrder.setReceiverAddress(defaultAddressByUserId.getAddress());
        tbKillOrder.setReceiver(defaultAddressByUserId.getContact());



/*        List<TbKillOrder> killOrderList = (List<TbKillOrder>) redisTemplate.boundHashOps("killOrder").get(userId);
        if(killOrderList==null){
            killOrderList=new ArrayList<>();
        }
        killOrderList.add(tbKillOrder);*/
        redisTemplate.boundHashOps("killOrder").put(userId,tbKillOrder);
/*        if(killOrderList!=null&&killOrderList.size()>0){
            System.out.println("以下是生成的预订单：===========================");
            for (TbKillOrder killOrder : killOrderList) {
                System.out.println(killOrder);
            }
        }*/
        TbKillOrder tbKillOrder1 = (TbKillOrder) redisTemplate.boundHashOps("killOrder").get(userId);


        //假设支付成功了
/*
        Map<String,String> map = new HashMap<>();
        map.put("orderId",tbKillOrder.getId()+"");
        rabbitTemplate.convertAndSend("queuekey", map);
*/


        System.out.println("1️以下是reids中的预订单："+tbKillOrder1);



        return new Result(true, 001, "提交成功");




    }



    //将商品库存预热到redis
    @RequestMapping("/goodsStockyure")
    public void goodsStockyure() {

        List<TbKillGoods> list = tbKillGoodsService.findList();
        String s1  = JSON.toJSONString(list);
        //根据key发送到对应的队列

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("killgoods", s1);
        map.put("killOrder", "");
        //根据key发送到对应的队列
        rabbitTemplate.convertAndSend("queuekey", map);

    }


    //根据秒杀商品id获取redis中秒杀商品列表中对应的商品库存
    @RequestMapping("/getStockById")
    public Long getStockById(Long id) {

        Integer stock = (Integer) redisTemplate.opsForHash().get("goodsStock", id + "");
        System.out.println("该商品库存剩余："+stock);
        return Long.parseLong(stock + "");
    }

    //获取秒杀倒计时时间
    @RequestMapping("/getStartTimeById")
    public Long getStartTimeById(Long id) {
        Long startTime = tbKillGoodsService.getStartTimeById(id);
        return startTime;
    }


}

