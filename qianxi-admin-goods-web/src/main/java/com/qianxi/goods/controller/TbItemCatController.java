package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.pojo.TbItemCat;
import com.qianxi.service.TbItemCatService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbItemCat")
@RestController
public class TbItemCatController {

    @Reference
    private TbItemCatService tbItemCatService;

    @RequestMapping("findAll")
    public List<TbItemCat> findAll() {
        return tbItemCatService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbItemCat
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbItemCat> pageList(@RequestBody(required = false) TbItemCat tbItemCat,
                                        @RequestParam(defaultValue = "3") int pageSize,
                                        @RequestParam(defaultValue = "1") int pageNum){

        return tbItemCatService.pageList(tbItemCat,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbItemCat
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbItemCat tbItemCat){
        try {
            tbItemCatService.save(tbItemCat);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbItemCatService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbItemCat getById(Long id){
        return tbItemCatService.getById(id);
    }

}
