package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.pojo.TbKillGoods;
import com.qianxi.service.TbKillGoodsService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbKillGoods")
@RestController
public class TbKillGoodsController {

    @Reference
    private TbKillGoodsService tbKillGoodsService;

    @RequestMapping("findAll")
    public List<TbKillGoods> findAll() {
        return tbKillGoodsService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbKillGoods
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbKillGoods> pageList(@RequestBody(required = false) TbKillGoods tbKillGoods,
                                        @RequestParam(defaultValue = "3") int pageSize,
                                        @RequestParam(defaultValue = "1") int pageNum){

        return tbKillGoodsService.pageList(tbKillGoods,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbKillGoods
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbKillGoods tbKillGoods){
        try {
            tbKillGoodsService.save(tbKillGoods);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbKillGoodsService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbKillGoods getById(Long id){
        return tbKillGoodsService.getById(id);
    }

}
