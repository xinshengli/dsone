package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.pojo.TbComment;
import com.qianxi.service.TbCommentService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbComment")
@RestController
public class TbCommentController {

    @Reference
    private TbCommentService tbCommentService;

    @RequestMapping("findAll")
    public List<TbComment> findAll() {
        return tbCommentService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbComment
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbComment> pageList(@RequestBody(required = false) TbComment tbComment,
                                        @RequestParam(defaultValue = "3") int pageSize,
                                        @RequestParam(defaultValue = "1") int pageNum){

        return tbCommentService.pageList(tbComment,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbComment
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbComment tbComment){
        try {
            tbCommentService.save(tbComment);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbCommentService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbComment getById(Long id){
        return tbCommentService.getById(id);
    }

}
