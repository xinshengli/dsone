package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import com.qianxi.pojo.TbItem;
import com.qianxi.service.TbItemService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/item")
public class TbItemController {

    @Reference
    private TbItemService tbItemService;

    @RequestMapping(value = "/findAll", method = RequestMethod.POST)
    public List<TbItem> findAll() {
        return tbItemService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbItem
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbItem> pageList(@RequestBody(required = false) TbItem tbItem,
                                            @RequestParam(defaultValue = "3") Integer pageSize,
                                            @RequestParam(defaultValue = "1") Integer pageNum){

        return tbItemService.pageList(tbItem,pageSize,pageNum);
    }

    /**
     * 新增修改-----注册
     * @param tbItem
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbItem tbItem){
        try {
            tbItemService.saveTbItem(tbItem);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(@RequestBody Long[] ids){
        try {
            tbItemService.deleteTbItem(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbItem getById(Long id){
        return tbItemService.getById(id);
    }



    @RequestMapping("getByGoodsId")
    public List<TbItem> getByGoodsId(Long id){


        List<TbItem> tbItemList = tbItemService.getByGoodsId(id);
        for (TbItem tbItem : tbItemList) {
            String spec = tbItem.getSpec();
            System.out.println(spec);
        }
        return tbItemList;
    }
}
