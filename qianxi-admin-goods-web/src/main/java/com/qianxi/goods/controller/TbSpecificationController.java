package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.pojo.TbSpecification;
import com.qianxi.service.TbSpecificationService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbSpecification")
@RestController
public class TbSpecificationController {

    @Reference
    private TbSpecificationService tbSpecificationService;

    @RequestMapping("findAll")
    public List<TbSpecification> findAll() {
        return tbSpecificationService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbSpecification
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbSpecification> pageList(@RequestBody(required = false) TbSpecification tbSpecification,
                                        @RequestParam(defaultValue = "3") int pageSize,
                                        @RequestParam(defaultValue = "1") int pageNum){

        return tbSpecificationService.pageList(tbSpecification,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbSpecification
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbSpecification tbSpecification){
        try {
            tbSpecificationService.save(tbSpecification);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbSpecificationService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbSpecification getById(Long id){
        return tbSpecificationService.getById(id);
    }

}
