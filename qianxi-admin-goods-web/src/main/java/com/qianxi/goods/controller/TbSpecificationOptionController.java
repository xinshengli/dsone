package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.pojo.TbSpecificationOption;
import com.qianxi.service.TbSpecificationOptionService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbSpecificationOption")
@RestController
public class TbSpecificationOptionController {

    @Reference
    private TbSpecificationOptionService tbSpecificationOptionService;

    @RequestMapping("findAll")
    public List<TbSpecificationOption> findAll() {
        return tbSpecificationOptionService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbSpecificationOption
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbSpecificationOption> pageList(@RequestBody(required = false) TbSpecificationOption tbSpecificationOption,
                                        @RequestParam(defaultValue = "3") int pageSize,
                                        @RequestParam(defaultValue = "1") int pageNum){

        return tbSpecificationOptionService.pageList(tbSpecificationOption,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbSpecificationOption
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbSpecificationOption tbSpecificationOption){
        try {
            tbSpecificationOptionService.save(tbSpecificationOption);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbSpecificationOptionService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbSpecificationOption getById(Long id){
        return tbSpecificationOptionService.getById(id);
    }

}
