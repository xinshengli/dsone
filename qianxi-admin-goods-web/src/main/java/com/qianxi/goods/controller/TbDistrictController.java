package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.pojo.TbDistrict;
import com.qianxi.service.TbDistrictService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbDistrict")
@RestController
public class TbDistrictController {

    @Reference
    private TbDistrictService tbDistrictService;

    @RequestMapping("findAll")
    public List<TbDistrict> findAll() {
        return tbDistrictService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbDistrict
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbDistrict> pageList(@RequestBody(required = false) TbDistrict tbDistrict,
                                        @RequestParam(defaultValue = "3") int pageSize,
                                        @RequestParam(defaultValue = "1") int pageNum){

        return tbDistrictService.pageList(tbDistrict,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbDistrict
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbDistrict tbDistrict){
        try {
            tbDistrictService.save(tbDistrict);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(String[] ids){
        try {
            tbDistrictService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbDistrict getById(String id){
        return tbDistrictService.getById(id);
    }

}
