package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import com.qianxi.pojo.TbGoods;
import com.qianxi.service.TbGoodsService;


import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/goods")
public class TbGoodsController {

    @Reference
    private TbGoodsService tbGoodsService;

    @RequestMapping(value = "/findAll", method = RequestMethod.POST)
    public List<TbGoods> findAll() {
        return tbGoodsService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbGoods
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbGoods> pageList(@RequestBody(required = false) TbGoods tbGoods,
                                            @RequestParam(defaultValue = "3") Integer pageSize,
                                            @RequestParam(defaultValue = "1") Integer pageNum){

        return tbGoodsService.pageList(tbGoods,pageSize,pageNum);
    }

    /**
     * 新增修改-----注册
     * @param tbGoods
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbGoods tbGoods){
        try {
            tbGoodsService.saveTbGoods(tbGoods);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(@RequestBody Long[] ids){
        try {
            tbGoodsService.deleteTbGoods(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbGoods getById(Long id){
        return tbGoodsService.getById(id);
    }

}
