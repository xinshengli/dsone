package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import com.qianxi.pojo.TbKillChang;
import com.qianxi.service.TbKillchangService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/killgoods")
public class TbKillchangController {

    @Reference
    private TbKillchangService tbKillchangService;

    @RequestMapping("findAll")
    public List<TbKillChang> findAll() {
        return tbKillchangService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbKillchang
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbKillChang> pageList(@RequestBody(required = false) TbKillChang tbKillchang,
                                            @RequestParam(defaultValue = "3") Integer pageSize,
                                            @RequestParam(defaultValue = "1") Integer pageNum){

        return tbKillchangService.pageList(tbKillchang,pageSize,pageNum);
    }

    /**
     * 新增修改-----注册
     * @param tbKillchang
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbKillChang tbKillchang){
        try {
            //这里先获取时间
            Date date=new Date();
            //new一个日期格式，大写HH是24小时制，小写hh是12小时制
            SimpleDateFormat temp=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //格式化时间，得到的是String类型
            String date2=temp.format(date);
            tbKillchang.setCreateTime(date2);
            String startTime = tbKillchang.getStartTime();
            String endTime = tbKillchang.getEndTime();
            startTime=startTime.replace("T"," ");
            startTime=startTime.replace(".000Z","");
            tbKillchang.setStartTime(startTime);
            endTime=endTime.replace("T"," ");
            endTime=endTime.replace(".000Z","");
            tbKillchang.setEndTime(endTime);
            tbKillchangService.saveKillChang(tbKillchang);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(@RequestBody Long[] ids){
        try {
            tbKillchangService.deleteKillChang(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbKillChang getById(Long id){
        return tbKillchangService.getById(id);
    }

}
