package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.pojo.TbGoodsDesc;
import com.qianxi.service.TbGoodsDescService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbGoodsDesc")
@RestController
public class TbGoodsDescController {

    @Reference
    private TbGoodsDescService tbGoodsDescService;

    @RequestMapping("findAll")
    public List<TbGoodsDesc> findAll() {
        return tbGoodsDescService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbGoodsDesc
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbGoodsDesc> pageList(@RequestBody(required = false) TbGoodsDesc tbGoodsDesc,
                                        @RequestParam(defaultValue = "3") int pageSize,
                                        @RequestParam(defaultValue = "1") int pageNum){

        return tbGoodsDescService.pageList(tbGoodsDesc,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbGoodsDesc
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbGoodsDesc tbGoodsDesc){
        try {
            tbGoodsDescService.save(tbGoodsDesc);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbGoodsDescService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbGoodsDesc getById(Long id){
        return tbGoodsDescService.getById(id);
    }

}
