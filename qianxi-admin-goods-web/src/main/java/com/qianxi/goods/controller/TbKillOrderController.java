package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.pojo.TbKillOrder;
import com.qianxi.service.TbKillOrderService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbKillOrder")
@RestController
public class TbKillOrderController {

    @Reference
    private TbKillOrderService tbKillOrderService;

    @RequestMapping("findAll")
    public List<TbKillOrder> findAll() {
        return tbKillOrderService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbKillOrder
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbKillOrder> pageList(@RequestBody(required = false) TbKillOrder tbKillOrder,
                                        @RequestParam(defaultValue = "3") int pageSize,
                                        @RequestParam(defaultValue = "1") int pageNum){

        return tbKillOrderService.pageList(tbKillOrder,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbKillOrder
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbKillOrder tbKillOrder){
        try {
            tbKillOrderService.save(tbKillOrder);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbKillOrderService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbKillOrder getById(Long id){
        return tbKillOrderService.getById(id);
    }

}
