package com.qianxi.goods.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.pojo.TbPayLog;
import com.qianxi.service.TbPayLogService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbPayLog")
@RestController
public class TbPayLogController {

    @Reference
    private TbPayLogService tbPayLogService;

    @RequestMapping("findAll")
    public List<TbPayLog> findAll() {
        return tbPayLogService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbPayLog
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbPayLog> pageList(@RequestBody(required = false) TbPayLog tbPayLog,
                                        @RequestParam(defaultValue = "3") int pageSize,
                                        @RequestParam(defaultValue = "1") int pageNum){

        return tbPayLogService.pageList(tbPayLog,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbPayLog
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbPayLog tbPayLog){
        try {
            tbPayLogService.save(tbPayLog);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(String[] ids){
        try {
            tbPayLogService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbPayLog getById(String id){
        return tbPayLogService.getById(id);
    }

}
