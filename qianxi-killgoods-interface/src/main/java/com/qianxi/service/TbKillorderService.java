package com.qianxi.service;

import com.qianxi.entity.PageResult;
import com.qianxi.pojo.TbKillOrder;

import java.util.List;

public interface TbKillorderService {
    public List<TbKillOrder> findAll();

    PageResult<TbKillOrder> pageList(TbKillOrder tbKillOrder, Integer pageSize, Integer pageNum);

    void saveTbKillOrder(TbKillOrder tbKillOrder);

    void deleteTbKillOrder(Long[] ids);

    TbKillOrder getById(Long id);
}
