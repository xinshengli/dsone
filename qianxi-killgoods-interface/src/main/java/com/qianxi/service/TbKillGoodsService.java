package com.qianxi.service;

import com.qianxi.pojo.TbKillGoods;

import java.util.List;

public interface TbKillGoodsService {
    /**
     * 返回当前正在参与秒杀的商品
     * @return
     */
    public List<TbKillGoods> findList();

    /**
     * 返回当前正在参与秒杀的商品
     * @return
     */
    public void updateByPrimaryKey(TbKillGoods tbKillGoods);



    /**
     * 根据商品id获取商品剩余库存的接口
     * @return
     */
    public Long getStockById(Long id);

    /**
     * 获取秒杀倒计时时间接口
     * @return
     */
    public Long getStartTimeById(Long id);


    public TbKillGoods getById(Long id);
}
