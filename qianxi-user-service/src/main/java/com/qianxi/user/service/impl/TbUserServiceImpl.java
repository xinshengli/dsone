package com.qianxi.user.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qianxi.entity.PageResult;
import com.qianxi.mapper.TbAddressMapper;
import com.qianxi.mapper.TbUserMapper;
import com.qianxi.pojo.TbUser;
import com.qianxi.pojo.TbUserExample;
import com.qianxi.user.service.TbUserService;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbUserServiceImpl implements TbUserService {

    @Resource
    private TbUserMapper tbUserMapper;

    @Override
    public List<TbUser> findAll() {
        return tbUserMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbUser> pageList(TbUser tbAddress, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbUserExample example = new TbUserExample();
        TbUserExample.Criteria criteria = example.createCriteria();
        if(tbAddress !=null){

        }

        Page<TbUser> page = (Page<TbUser>) tbUserMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbUser tbUser) {
        Long id =tbUser.getId();
        if(id == null || id == 0){
            tbUserMapper.insert(tbUser);
        } else{
            tbUserMapper.updateByPrimaryKey(tbUser);
        }
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbUserMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbUser getById(Long id) {
        return tbUserMapper.selectByPrimaryKey(id);
    }

    @Scheduled(cron = "0/5 * * * * ? ") // 间隔5秒执行
    public void taskCycle() {
        System.out.println("使用SpringMVC框架配置定时任务");
    }

}
