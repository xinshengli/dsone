package com.qianxi.bargain.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import com.qianxi.bargain.service.TbBargainListService;
import com.qianxi.pojo.TbBargainList;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbBargainList")
@RestController
public class TbBargainListController {

    @Reference
    private TbBargainListService tbBargainListService;

    @RequestMapping("findAll")
    public List<TbBargainList> findAll() {
        return tbBargainListService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbBargainList
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbBargainList> pageList(@RequestBody(required = false) TbBargainList tbBargainList,
                                              @RequestParam(defaultValue = "3") int pageSize,
                                              @RequestParam(defaultValue = "1") int pageNum){

        return tbBargainListService.pageList(tbBargainList,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbBargainList
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbBargainList tbBargainList){
        try {
            tbBargainListService.save(tbBargainList);
            return  new Result(true,100,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,200,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbBargainListService.deletebth(ids);
            return  new Result(true,100,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,200,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbBargainList getById(Long id){
        return tbBargainListService.getById(id);
    }

}
