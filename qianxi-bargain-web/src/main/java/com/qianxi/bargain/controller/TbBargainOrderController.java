package com.qianxi.bargain.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.bargain.service.TbBargainOrderService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import com.qianxi.pojo.TbBargainOrder;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbBargainOrder")
@RestController
public class TbBargainOrderController {

    @Reference
    private TbBargainOrderService tbBargainOrderService;

    @RequestMapping("findAll")
    public List<TbBargainOrder> findAll() {
        return tbBargainOrderService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbBargainOrder
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbBargainOrder> pageList(@RequestBody(required = false) TbBargainOrder tbBargainOrder,
                                               @RequestParam(defaultValue = "3") int pageSize,
                                               @RequestParam(defaultValue = "1") int pageNum){

        return tbBargainOrderService.pageList(tbBargainOrder,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbBargainOrder
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbBargainOrder tbBargainOrder){
        try {
            tbBargainOrderService.save(tbBargainOrder);
            return  new Result(true,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbBargainOrderService.deletebth(ids);
            return  new Result(true,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbBargainOrder getById(Long id){
        return tbBargainOrderService.getById(id);
    }

}
