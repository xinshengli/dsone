package com.qianxi.bargain.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.bargain.service.TbBargainRecordService;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import com.qianxi.pojo.TbBargainRecord;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:02
 */
@RequestMapping("tbBargainRecord")
@RestController
public class TbBargainRecordController {

    @Reference
    private TbBargainRecordService tbBargainRecordService;

    @RequestMapping("findAll")
    public List<TbBargainRecord> findAll() {
        return tbBargainRecordService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param tbBargainRecord
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbBargainRecord> pageList(@RequestBody(required = false) TbBargainRecord tbBargainRecord,
                                                @RequestParam(defaultValue = "3") int pageSize,
                                                @RequestParam(defaultValue = "1") int pageNum){

        return tbBargainRecordService.pageList(tbBargainRecord,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param tbBargainRecord
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbBargainRecord tbBargainRecord){
        try {
            tbBargainRecordService.save(tbBargainRecord);
            return  new Result(true,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbBargainRecordService.deletebth(ids);
            return  new Result(true,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbBargainRecord getById(Long id){
        return tbBargainRecordService.getById(id);
    }

    @RequestMapping("kanjia")
    public TbBargainRecord getById1(Long barId){
        return tbBargainRecordService.getkanjia(barId);
    }
}
