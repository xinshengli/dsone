package com.qianxi.bargain.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.bargain.kanjia.Factory.BargainFactory;
import com.qianxi.bargain.kanjia.Strategy.BargainStrategy;
import com.qianxi.bargain.kanjia.Strategy.Context;
import com.qianxi.bargain.service.TbBargainGoodsService;
import com.qianxi.bargain.service.TbBargainListService;
import com.qianxi.bargain.service.TbBargainRecordService;
import com.qianxi.entity.Result;
import com.qianxi.pojo.TbAddress;
import com.qianxi.pojo.TbBargainGoods;
import com.qianxi.pojo.TbBargainList;
import com.qianxi.pojo.TbBargainRecord;
import com.qianxi.user.service.TbAddressService;
import com.qianxi.utils.BigDecimalArith;
import com.qianxi.utils.DateUtils;
import org.omg.CORBA.TRANSACTION_MODE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RequestMapping("kanjia")
@RestController
public class BargainController {
    @Reference
    private TbBargainGoodsService tbBargainGoodsService;

    @Reference
    private TbBargainListService tbBargainListService;

    @Reference
    private TbBargainRecordService tbBargainRecordService;

    @Reference
    private TbAddressService tbAddressService;

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("AddKanJiaJiLu")
    public Result JinDaoKanJia(Long barGoodsId){

        //首先先判断用户是否登录，未登录提示
        String userId = SecurityContextHolder.getContext().getAuthentication().getName();
        if ("anonymousUser".equals(userId)) {//如果未登录
            return new Result(false, 200, "用户未登录");
        }

        //新建一个砍价信息
        TbBargainList tbBargainList=new TbBargainList();


        //根据用户名获取到用户默认的收货地址
        TbAddress defaultAddress= tbAddressService.getDefaultAddressByUserId(userId);

        //对砍价信息进行赋值
        tbBargainList.setBarGoodsId(Integer.parseInt(barGoodsId+""));
        //假设用户自己砍价完毕  设置砍价数量为1
        tbBargainList.setBarNum(1);

        //根据砍价商品id获取对象
        TbBargainGoods tbBargainGoods = tbBargainGoodsService.getById(barGoodsId);

        tbBargainList.setBarResidue(tbBargainGoods.getBarPeopleNum()-1);
        //将默认地址赋值给砍价信息对象
        tbBargainList.setUserAddress(defaultAddress.getAddress());



        //获取当前时间
        Date date = new Date();

        //当前时间的赋值
        String startTime = DateUtils.dateToString(new Date(), "yyyy-MM-dd HH:mm:ss");
        tbBargainList.setStartTime(startTime);
        long time = date.getTime();
        Integer barYouxiaoTime = tbBargainGoods.getBarYouxiaoTime();

        //中间根据砍价商品的砍价持续时长算出开始时间和结束时间
        //结束时间的赋值
        Calendar calendar=Calendar.getInstance();
        calendar.setTimeInMillis(time+barYouxiaoTime*1000);
        Date time1 = calendar.getTime();
        String endTime = DateUtils.dateToString(time1, "yyyy-MM-dd HH:mm:ss");
        tbBargainList.setEndTime(endTime);

        tbBargainList.setStatus("1");


        String barPrice = tbBargainGoods.getBarPrice();
        String mul = BigDecimalArith.mul(barPrice, 0.75 + "");
        String s = BigDecimalArith.subAndReturn3Digit(barPrice, mul);

        //将自己砍掉之后的价格赋值
        tbBargainList.setBarPrice(s);

        tbBargainList.setUserId(userId);

        //对砍价列表add
        tbBargainListService.save(tbBargainList);

        TbBargainRecord tbBargainRecord=new TbBargainRecord();
        tbBargainRecord.setUserId(userId);
        tbBargainRecord.setBarId(Integer.parseInt(barGoodsId+""));
        tbBargainRecord.setGoodsId(tbBargainGoods.getGoodsId());
        tbBargainRecord.setBarPrice(mul);
        tbBargainRecord.setBarTime(DateUtils.getCurrentTime());
        tbBargainRecordService.save(tbBargainRecord);

        //用砍价算法把剩余价格根据次数用算法算出集合 放入reids缓存
        Context context=new Context();
        BargainFactory bargainFactory = new BargainFactory();
        BargainStrategy pinduoduo = bargainFactory.createStrategy("pinduoduo");
        context.setBargainStrategy(pinduoduo);
        //获取到集合
        List<Double> bargainList = context.contextInterface(Integer.parseInt(s)*100, tbBargainList.getBarResidue(),tbBargainGoods.getBarMoneyMin(), Integer.parseInt(s)*100);

        redisTemplate.opsForHash().put("kanJiaList",userId,bargainList);


        return new Result(true,001,"成功");
    }

    @RequestMapping("yaoqing")
    public Result yaoqing(String userId,Long barGoodsId){
        TbBargainGoods tbBargainGoods = tbBargainGoodsService.getById(barGoodsId);
        userId = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Double> bargainList= (List<Double>) redisTemplate.opsForHash().get("kanJiaList", userId);
        if(bargainList.size()>0&&bargainList!=null){

            for (int i = 0; i < bargainList.size(); i++) {
                System.out.println(bargainList.get(i));


                Double aDouble = bargainList.get(i);
                TbBargainRecord tbBargainRecord=new TbBargainRecord();
                tbBargainRecord.setUserId("模拟用户"+(i+1));
                tbBargainRecord.setBarId(Integer.parseInt(barGoodsId+""));
                tbBargainRecord.setGoodsId(tbBargainGoods.getGoodsId());
                tbBargainRecord.setBarPrice(aDouble.toString());
                tbBargainRecord.setBarTime(DateUtils.getCurrentTime());
                tbBargainRecordService.save(tbBargainRecord);
            }
        }
        redisTemplate.opsForHash().put("kanJiaList", userId,"");
        return new Result(true,001,"砍价到手了");
    }
}
