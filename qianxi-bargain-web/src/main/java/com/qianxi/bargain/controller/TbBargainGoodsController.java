package com.qianxi.bargain.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.qianxi.entity.PageResult;
import com.qianxi.entity.Result;
import com.qianxi.pojo.TbBargainGoods;
import com.qianxi.bargain.service.TbBargainGoodsService;
import com.qianxi.utils.Down;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.util.List;

@RequestMapping("TbBargainGoods")
@RestController
public class TbBargainGoodsController {
    @Reference
    private TbBargainGoodsService tbBargainGoodsService;
    @RequestMapping("Down")
    public void downloadLocal(HttpServletResponse response) throws FileNotFoundException, FileNotFoundException {
        Down down = new Down();
        down.downloadLocal(response);
    }
    @RequestMapping("findAll")
    public List<TbBargainGoods> findAll() {
        return tbBargainGoodsService.findAll();
    }

    /**
     *
     * 模糊分页
     * @param
     * @param pageSize
     * @param pageNum
     * @return
     */
    @RequestMapping("pageList")
    public PageResult<TbBargainGoods> pageList(@RequestBody(required = false) TbBargainGoods tbBargainGoods,
                                            @RequestParam(defaultValue = "3") int pageSize,
                                            @RequestParam(defaultValue = "1") int pageNum){

        return tbBargainGoodsService.pageList(tbBargainGoods,pageSize,pageNum);
    }

    /**
     * 新增修改
     * @param
     * @return
     */
    @RequestMapping("save")
    public Result save(@RequestBody TbBargainGoods tbBargainGoods){
        try {
            tbBargainGoodsService.save(tbBargainGoods);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("deletebth")
    public Result deletebth(Long[] ids){
        try {
            tbBargainGoodsService.deletebth(ids);
            return  new Result(true,001,"操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(true,002,"操作失败");
        }
    }

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    @RequestMapping("getById")
    public TbBargainGoods getById(Long id){
        return tbBargainGoodsService.getById(id);
    }

}
