package com.qianxi.bargain.kanjia.Strategy;

import java.util.ArrayList;
import java.util.List;

public class ConcreteStrategyBargainTwo implements BargainStrategy {
    public static final double TIMES = 3.1;

    /**
     * 砍价合法性校验
     * @param money
     * @param count
     * @return
     */
    public static boolean isRight(int money,int count,int min,int max){
        double avg = money/count;
        //小于最小金额
        if (avg<min) {
            return false;
        }else if (avg>max) {
            return false;
        }
        return true;
    }

    /**
     * 随机分配一个金额
     * @param
     * @param minS：最小金额
     * @param maxS：最大金额
     * @param count
     * @return
     */
    public static int randomReducePrice(int money,int minS,int maxS,int count){
        //若只有一个，直接返回
        if (count==1) {
            return money;
        }
        //如果最大金额和最小金额相等，直接返回金额
        if (minS==maxS) {
            return minS;
        }
        int max=maxS>money?money:maxS;
        //分配砍价正确情况，允许砍价的最大值
        int maxY = money-(count-1)*minS;
        //分配砍价正确情况，允许砍价最小值
        int minY = money-(count-1)*maxS;
        //随机产生砍价的最小值
        int min = minS>minY?minS:minY;
        //随机产生砍价的最大值
        max = max>maxY?maxY:max;
        //随机产生一个砍价
        return (int)Math.rint(Math.random()*(max-min) +min);
    }
    @Override
    public List<Double> algorithmMethod(int money, int count, int min, int max) {
        if (!isRight(money, count,min,max)) {
            return new ArrayList<>();
        }
        //红包列表
        List<Double> list = new ArrayList<>();
        //每个红包的最大的金额为平均金额的TIMES倍
        max = (int)(money*TIMES/count);
        //分配红包
        int sum = 0;

        for(int i=0;i<count;i++){
            int one = randomReducePrice(money, min, max, count-i);
            list.add(one/100.0);
            money-=one;
            sum += one;
        }
        System.out.println("sum:"+sum);
        return list;
    }
}
