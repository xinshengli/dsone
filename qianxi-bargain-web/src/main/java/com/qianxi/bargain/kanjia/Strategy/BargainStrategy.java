package com.qianxi.bargain.kanjia.Strategy;

import java.util.List;

public  interface BargainStrategy {
    List<Double> algorithmMethod(int money, int count, int min, int max);
}