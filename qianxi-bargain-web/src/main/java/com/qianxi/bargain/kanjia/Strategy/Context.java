package com.qianxi.bargain.kanjia.Strategy;

import java.util.List;

public class Context {
    private BargainStrategy bargainStrategy;

    public BargainStrategy getBargainStrategy() {
        return bargainStrategy;
    }

    public void setBargainStrategy(BargainStrategy bargainStrategy) {
        this.bargainStrategy = bargainStrategy;
    }


    public List<Double> contextInterface(int money, int count, int min, int max) {
         return bargainStrategy.algorithmMethod(money, count, min, max);
    }
}
