package com.qianxi.bargain.kanjia.Factory;

import com.qianxi.bargain.kanjia.Strategy.BargainStrategy;
import com.qianxi.bargain.kanjia.Strategy.ConcreteStrategyBargainOne;
import com.qianxi.bargain.kanjia.Strategy.ConcreteStrategyBargainThree;
import com.qianxi.bargain.kanjia.Strategy.ConcreteStrategyBargainTwo;

public class BargainFactory {

    public BargainStrategy createStrategy(String strName){
        if (strName == null){
            return null;
        }
        if ("pinduoduo".equalsIgnoreCase(strName)){
            return new ConcreteStrategyBargainOne();
        } else if ("suiji".equalsIgnoreCase(strName)){
            return new ConcreteStrategyBargainTwo();
        }else if("ceshi".equalsIgnoreCase(strName)){
            return new ConcreteStrategyBargainThree();
        }
        return null;
    }
}
