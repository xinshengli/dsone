package com.rabbitmq.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

import java.io.IOException;

public class RecvHandler implements MessageListener {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    public void onMessage(Message msg) {
        try {
            // msg就是rabbitmq传来的消息，需要的同学自己打印看一眼
            // 使用jackson解析
            JsonNode jsonData = MAPPER.readTree(msg.getBody());
            System.out.println("我是可爱的小猪,我的id是" + jsonData.get("id").asText() + ",我的名字是" + jsonData.get("name").asText());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class animal{

    }

    @Test
    public void test(){
/*        int a=10;
        int b=0,c;
        if(a>50){
            b=9;
        }
        c=b+a;
        System.out.println(c);*/

/*        int three=3;
        char one='1';
        char four=(char)(three+one);
        System.out.println(four);*/
/*
        int a=7;
        System.out.println(a%3);*/

        String str="null";
        if(str==null){
            System.out.println("null");
        }else if(str.length()==0){
            System.out.println("zero");
        }else{
            System.out.println("some");
        }
    }
}
