package com.qianxi.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qianxi.entity.PageResult;
import com.qianxi.mapper.TbKillOrderMapper;
import com.qianxi.pojo.TbKillOrder;
import com.qianxi.pojo.TbKillOrderExample;
import com.qianxi.service.TbKillorderService;

import javax.annotation.Resource;
import java.util.List;
@Service(timeout = 5000,retries = 0)
public class TbKillorderServiceImpl implements TbKillorderService {
    @Resource
    private TbKillOrderMapper tbKillOrderMapper;
    @Override
    public List<TbKillOrder> findAll() {


        List<TbKillOrder> tbKillOrders = tbKillOrderMapper.selectByExample(null);


        return tbKillOrders;
    }

    @Override
    public PageResult<TbKillOrder> pageList(TbKillOrder tbKillOrder, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbKillOrderExample example = new TbKillOrderExample();
        TbKillOrderExample.Criteria criteria = example.createCriteria();
        if(tbKillOrder!=null){

        }
        Page<TbKillOrder> page = (Page<TbKillOrder>) tbKillOrderMapper.selectByExample(example);
        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void saveTbKillOrder(TbKillOrder tbKillOrder) {

            tbKillOrderMapper.insert(tbKillOrder);

    }

    @Override
    public void deleteTbKillOrder(Long[] ids) {
        for (Long id : ids) {
            tbKillOrderMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbKillOrder getById(Long id) {
        return tbKillOrderMapper.selectByPrimaryKey(id);
    }
}
