package com.qianxi.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.qianxi.mapper.TbKillChangMapper;
import com.qianxi.mapper.TbKillGoodsMapper;
import com.qianxi.pojo.TbKillChang;
import com.qianxi.pojo.TbKillGoods;
import com.qianxi.pojo.TbKillGoodsExample;
import com.qianxi.service.TbKillGoodsService;
import com.qianxi.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service(timeout = 5000,retries = 0)
public class TbKillGoodsServiceImpl implements TbKillGoodsService {

    @Resource
    private TbKillGoodsMapper tbKillGoodsMapper;

    @Resource
    private TbKillChangMapper tbKillChangMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public List<TbKillGoods> findList() {

        TbKillGoodsExample example=new TbKillGoodsExample();
        TbKillGoodsExample.Criteria criteria = example.createCriteria();
        criteria.andStockGreaterThan(0);
        return tbKillGoodsMapper.selectByExample(example);
    }

    @Override
    public void updateByPrimaryKey(TbKillGoods tbKillGoods) {
        tbKillGoodsMapper.updateByPrimaryKey(tbKillGoods);
    }

    @Override
    public Long getStockById(Long id) {

        Integer stock = (Integer) redisTemplate.opsForHash().get("goodsStock", id + "");

        long stock2 = Long.parseLong(stock+"");
        return stock2;
    }

    @Override
    public Long getStartTimeById(Long id) {

        TbKillGoodsExample example=new TbKillGoodsExample();
        TbKillGoodsExample.Criteria criteria = example.createCriteria();
        criteria.andItemIdEqualTo(Integer.parseInt(id+""));
        TbKillGoods tbKillGoods2=new TbKillGoods();
        List<TbKillGoods> tbKillGoodsList = tbKillGoodsMapper.selectByExample(example);
        if(tbKillGoodsList.size()>0){
            for (TbKillGoods tbKillGoods : tbKillGoodsList) {
                tbKillGoods2=tbKillGoods;
                break;
            }
        }
        Long killChangId = tbKillGoods2.getKillChangId();
        TbKillChang tbKillChang = tbKillChangMapper.selectByPrimaryKey(killChangId);
        String startTime = tbKillChang.getStartTime();
        String currentTime = DateUtils.getCurrentTime();

        Date startDate = DateUtils.StringToDate(startTime, "yyyy-MM-dd hh:mm:ss");
        Date currentDate = DateUtils.StringToDate(currentTime, "yyyy-MM-dd hh:mm:ss");
        int c = (int) (startDate.getTime() - currentDate.getTime());
        return Long.parseLong((c/1000)+"");
    }

    @Override
    public TbKillGoods getById(Long id) {

        TbKillGoodsExample example = new TbKillGoodsExample();
        TbKillGoodsExample.Criteria criteria = example.createCriteria();
        criteria.andItemIdEqualTo(Integer.parseInt(id+""));

        List<TbKillGoods> tbKillGoodsList = tbKillGoodsMapper.selectByExample(example);
        if(tbKillGoodsList.size()>0){
            for (TbKillGoods tbKillGoods : tbKillGoodsList) {
                return tbKillGoods;
            }
        }
        return null;
    }

}