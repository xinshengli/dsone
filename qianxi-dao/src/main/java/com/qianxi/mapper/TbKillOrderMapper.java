package com.qianxi.mapper;

import com.qianxi.pojo.TbKillOrder;
import com.qianxi.pojo.TbKillOrderExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbKillOrderMapper {
    int countByExample(TbKillOrderExample example);

    int deleteByExample(TbKillOrderExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbKillOrder record);

    int insertSelective(TbKillOrder record);

    List<TbKillOrder> selectByExample(TbKillOrderExample example);

    TbKillOrder selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbKillOrder record, @Param("example") TbKillOrderExample example);

    int updateByExample(@Param("record") TbKillOrder record, @Param("example") TbKillOrderExample example);

    int updateByPrimaryKeySelective(TbKillOrder record);

    int updateByPrimaryKey(TbKillOrder record);
}