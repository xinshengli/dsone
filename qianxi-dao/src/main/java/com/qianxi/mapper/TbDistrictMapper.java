package com.qianxi.mapper;

import com.qianxi.pojo.TbDistrict;
import com.qianxi.pojo.TbDistrictExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbDistrictMapper {
    int countByExample(TbDistrictExample example);

    int deleteByExample(TbDistrictExample example);

    int deleteByPrimaryKey(String id);

    int insert(TbDistrict record);

    int insertSelective(TbDistrict record);

    List<TbDistrict> selectByExample(TbDistrictExample example);

    TbDistrict selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") TbDistrict record, @Param("example") TbDistrictExample example);

    int updateByExample(@Param("record") TbDistrict record, @Param("example") TbDistrictExample example);

    int updateByPrimaryKeySelective(TbDistrict record);

    int updateByPrimaryKey(TbDistrict record);
}