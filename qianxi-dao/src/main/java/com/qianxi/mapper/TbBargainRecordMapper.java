package com.qianxi.mapper;

import com.qianxi.pojo.TbBargainRecord;
import com.qianxi.pojo.TbBargainRecordExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbBargainRecordMapper {
    int countByExample(TbBargainRecordExample example);

    int deleteByExample(TbBargainRecordExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbBargainRecord record);

    int insertSelective(TbBargainRecord record);

    List<TbBargainRecord> selectByExample(TbBargainRecordExample example);
    TbBargainRecord kanjia(Long barId);
    TbBargainRecord selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbBargainRecord record, @Param("example") TbBargainRecordExample example);

    int updateByExample(@Param("record") TbBargainRecord record, @Param("example") TbBargainRecordExample example);

    int updateByPrimaryKeySelective(TbBargainRecord record);

    int updateByPrimaryKey(TbBargainRecord record);
}