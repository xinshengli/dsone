package com.qianxi.mapper;

import com.qianxi.pojo.TbKillGoods;
import com.qianxi.pojo.TbKillGoodsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbKillGoodsMapper {
    int countByExample(TbKillGoodsExample example);

    int deleteByExample(TbKillGoodsExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbKillGoods record);

    int insertSelective(TbKillGoods record);

    List<TbKillGoods> selectByExample(TbKillGoodsExample example);

    TbKillGoods selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbKillGoods record, @Param("example") TbKillGoodsExample example);

    int updateByExample(@Param("record") TbKillGoods record, @Param("example") TbKillGoodsExample example);

    int updateByPrimaryKeySelective(TbKillGoods record);

    int updateByPrimaryKey(TbKillGoods record);
}