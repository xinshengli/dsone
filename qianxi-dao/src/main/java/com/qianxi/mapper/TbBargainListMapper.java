package com.qianxi.mapper;

import java.util.List;

import com.qianxi.pojo.TbBargainList;
import com.qianxi.pojo.TbBargainListExample;
import org.apache.ibatis.annotations.Param;

public interface TbBargainListMapper {
    int countByExample(TbBargainListExample example);

    int deleteByExample(TbBargainListExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbBargainList record);

    int insertSelective(TbBargainList record);

    List<TbBargainList> selectByExample(TbBargainListExample example);

    TbBargainList selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbBargainList record, @Param("example") TbBargainListExample example);

    int updateByExample(@Param("record") TbBargainList record, @Param("example") TbBargainListExample example);

    int updateByPrimaryKeySelective(TbBargainList record);

    int updateByPrimaryKey(TbBargainList record);
}