package com.qianxi.mapper;

import com.qianxi.pojo.TbKillChang;
import com.qianxi.pojo.TbKillChangExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbKillChangMapper {
    int countByExample(TbKillChangExample example);

    int deleteByExample(TbKillChangExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbKillChang record);

    int insertSelective(TbKillChang record);

    List<TbKillChang> selectByExample(TbKillChangExample example);

    TbKillChang selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbKillChang record, @Param("example") TbKillChangExample example);

    int updateByExample(@Param("record") TbKillChang record, @Param("example") TbKillChangExample example);

    int updateByPrimaryKeySelective(TbKillChang record);

    int updateByPrimaryKey(TbKillChang record);
}