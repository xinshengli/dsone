package com.qianxi.mapper;

import com.qianxi.pojo.TbBargainOrder;
import com.qianxi.pojo.TbBargainOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbBargainOrderMapper {
    int countByExample(TbBargainOrderExample example);

    int deleteByExample(Long example);

    int insert(TbBargainOrder record);

    int insertSelective(TbBargainOrder record);

    List<TbBargainOrder> selectByExample(TbBargainOrderExample example);

    int updateByExampleSelective(@Param("record") TbBargainOrder record, @Param("example") TbBargainOrderExample example);

    int updateByExample(@Param("record") TbBargainOrder record, @Param("example") TbBargainOrderExample example);
}