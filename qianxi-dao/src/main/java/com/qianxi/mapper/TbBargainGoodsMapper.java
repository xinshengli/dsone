package com.qianxi.mapper;

import com.qianxi.pojo.TbBargainGoods;
import com.qianxi.pojo.TbBargainGoodsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbBargainGoodsMapper {
    int countByExample(TbBargainGoodsExample example);

    int deleteByExample(TbBargainGoodsExample example);

    int deleteByPrimaryKey(Long barGoodsId);

    int insert(TbBargainGoods record);

    int insertSelective(TbBargainGoods record);

    List<TbBargainGoods> selectByExample(TbBargainGoodsExample example);

    TbBargainGoods selectByPrimaryKey(Long barGoodsId);

    int updateByExampleSelective(@Param("record") TbBargainGoods record, @Param("example") TbBargainGoodsExample example);

    int updateByExample(@Param("record") TbBargainGoods record, @Param("example") TbBargainGoodsExample example);

    int updateByPrimaryKeySelective(TbBargainGoods record);

    int updateByPrimaryKey(TbBargainGoods record);
}