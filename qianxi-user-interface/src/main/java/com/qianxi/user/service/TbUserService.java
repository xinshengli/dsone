package com.qianxi.user.service;

import com.qianxi.entity.PageResult;
import com.qianxi.pojo.TbUser;

import java.util.List;

public interface TbUserService {

    public List<TbUser> findAll();

    PageResult<TbUser> pageList(TbUser tbUser, int pageSize, int pageNum);

    void save(TbUser tbUser);

    void deletebth(Long[] ids);

    TbUser getById(Long id);
}
