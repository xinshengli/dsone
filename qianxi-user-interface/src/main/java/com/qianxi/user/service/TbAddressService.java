package com.qianxi.user.service;

import com.qianxi.entity.PageResult;
import com.qianxi.pojo.TbAddress;

import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:00
 */

public interface TbAddressService {

    /**
     * 返回全部列表
     * @return
     */
    public List<TbAddress> findAll();

    PageResult<TbAddress> pageList(TbAddress tbAddress, int pageSize, int pageNum);

    void save(TbAddress tbAddress);

    void deletebth(Long[] ids);

    TbAddress getById(Long id);

    TbAddress getDefaultAddressByUserId(String userId);
}
