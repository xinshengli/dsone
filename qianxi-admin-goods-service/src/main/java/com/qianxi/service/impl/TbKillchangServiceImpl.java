package com.qianxi.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qianxi.entity.PageResult;
import com.qianxi.mapper.TbKillChangMapper;
import com.qianxi.pojo.TbKillChang;
import com.qianxi.pojo.TbKillChangExample;
import com.qianxi.service.TbKillchangService;

import javax.annotation.Resource;
import java.util.List;

@Service(timeout = 5000,retries = 0)
public class TbKillchangServiceImpl implements TbKillchangService {

    @Resource
    private TbKillChangMapper tbKillChangMapper;

    @Override
    public List<TbKillChang> findAll() {
        List<TbKillChang> tbKillchangs = tbKillChangMapper.selectByExample(null);
        return tbKillchangs;
    }

    @Override
    public PageResult<TbKillChang> pageList(TbKillChang tbKillchang, Integer pageSize, Integer pageNum) {

        PageHelper.startPage(pageNum,pageSize);

        TbKillChangExample example=new TbKillChangExample();
        TbKillChangExample.Criteria criteria = example.createCriteria();
        if(tbKillchang!=null){

        }
        Page<TbKillChang> page = (Page<TbKillChang>) tbKillChangMapper.selectByExample(example);
        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void saveKillChang(TbKillChang tbKillchang) {
        Long id = tbKillchang.getId();
        if(id==null||id==0){
            tbKillChangMapper.insert(tbKillchang);
        }else{
            tbKillChangMapper.updateByPrimaryKey(tbKillchang);
        }
    }

    @Override
    public void deleteKillChang(Long[] ids) {
        for (Long id : ids) {
            tbKillChangMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbKillChang getById(Long id) {
        return tbKillChangMapper.selectByPrimaryKey(id);
    }
}
