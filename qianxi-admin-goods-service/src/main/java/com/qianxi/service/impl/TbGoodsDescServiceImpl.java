package com.qianxi.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mysql.jdbc.StringUtils;
import com.qianxi.mapper.TbGoodsDescMapper;
import com.qianxi.pojo.TbGoodsDesc;
import com.qianxi.pojo.TbGoodsDescExample;
import com.qianxi.service.TbGoodsDescService;
import com.qianxi.entity.PageResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbGoodsDescServiceImpl implements TbGoodsDescService {

    @Resource
    private TbGoodsDescMapper tbGoodsDescMapper;

    @Override
    public List<TbGoodsDesc> findAll() {
        return tbGoodsDescMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbGoodsDesc> pageList(TbGoodsDesc tbGoodsDesc, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbGoodsDescExample example = new TbGoodsDescExample();
        TbGoodsDescExample.Criteria criteria = example.createCriteria();
        if(tbGoodsDesc !=null){

        }

        Page<TbGoodsDesc> page = (Page<TbGoodsDesc>) tbGoodsDescMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbGoodsDesc tbGoodsDesc) {
        Long id =tbGoodsDesc.getGoodsId();
        if(id == null || id == 0){
            tbGoodsDescMapper.insert(tbGoodsDesc);
        } else{
            tbGoodsDescMapper.updateByPrimaryKey(tbGoodsDesc);
        }
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbGoodsDescMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbGoodsDesc getById(Long id) {
        return tbGoodsDescMapper.selectByPrimaryKey(id);
    }
}
