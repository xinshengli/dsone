package com.qianxi.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mysql.jdbc.StringUtils;
import com.qianxi.mapper.TbKillGoodsMapper;
import com.qianxi.pojo.TbKillGoods;
import com.qianxi.pojo.TbKillGoodsExample;
import com.qianxi.service.TbKillGoodsService;
import com.qianxi.entity.PageResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbKillGoodsServiceImpl implements TbKillGoodsService {

    @Resource
    private TbKillGoodsMapper tbKillGoodsMapper;

    @Override
    public List<TbKillGoods> findAll() {
        return tbKillGoodsMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbKillGoods> pageList(TbKillGoods tbKillGoods, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbKillGoodsExample example = new TbKillGoodsExample();
        TbKillGoodsExample.Criteria criteria = example.createCriteria();
        if(tbKillGoods !=null){

        }

        Page<TbKillGoods> page = (Page<TbKillGoods>) tbKillGoodsMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbKillGoods tbKillGoods) {
        Long id =tbKillGoods.getId();
        if(id == null || id == 0){
            tbKillGoodsMapper.insert(tbKillGoods);
        } else{
            tbKillGoodsMapper.updateByPrimaryKey(tbKillGoods);
        }
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbKillGoodsMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbKillGoods getById(Long id) {
        return tbKillGoodsMapper.selectByPrimaryKey(id);
    }
}
