package com.qianxi.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mysql.jdbc.StringUtils;
import com.qianxi.mapper.TbItemCatMapper;
import com.qianxi.pojo.TbItemCat;
import com.qianxi.pojo.TbItemCatExample;
import com.qianxi.service.TbItemCatService;
import com.qianxi.entity.PageResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbItemCatServiceImpl implements TbItemCatService {

    @Resource
    private TbItemCatMapper tbItemCatMapper;

    @Override
    public List<TbItemCat> findAll() {
        return tbItemCatMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbItemCat> pageList(TbItemCat tbItemCat, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbItemCatExample example = new TbItemCatExample();
        TbItemCatExample.Criteria criteria = example.createCriteria();
        if(tbItemCat !=null){

        }

        Page<TbItemCat> page = (Page<TbItemCat>) tbItemCatMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbItemCat tbItemCat) {
        Long id =tbItemCat.getId();
        if(id == null || id == 0){
            tbItemCatMapper.insert(tbItemCat);
        } else{
            tbItemCatMapper.updateByPrimaryKey(tbItemCat);
        }
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbItemCatMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbItemCat getById(Long id) {
        return tbItemCatMapper.selectByPrimaryKey(id);
    }
}
