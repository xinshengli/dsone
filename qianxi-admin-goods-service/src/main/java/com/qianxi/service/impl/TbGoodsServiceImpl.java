package com.qianxi.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qianxi.entity.PageResult;
import com.qianxi.mapper.TbGoodsMapper;
import com.qianxi.pojo.TbGoods;
import com.qianxi.pojo.TbGoodsExample;
import com.qianxi.service.TbGoodsService;

import javax.annotation.Resource;
import java.util.List;
@Service(timeout = 5000,retries = 0)
public class TbGoodsServiceImpl implements TbGoodsService {
    @Resource
    private TbGoodsMapper tbGoodsMapper;

    @Override
    public List<TbGoods> findAll() {
        List<TbGoods> tbGoods = tbGoodsMapper.selectByExample(null);
        return tbGoods;
    }

    @Override
    public PageResult<TbGoods> pageList(TbGoods tbGoods, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum,pageSize);

        TbGoodsExample example=new TbGoodsExample();
        TbGoodsExample.Criteria criteria = example.createCriteria();
        if(tbGoods!=null){

        }
        Page<TbGoods> page = (Page<TbGoods>) tbGoodsMapper.selectByExample(example);
        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void saveTbGoods(TbGoods tbGoods) {
        Long id = tbGoods.getId();
        if(id==null||id==0){
            tbGoodsMapper.insert(tbGoods);
        }else{
            tbGoodsMapper.updateByPrimaryKey(tbGoods);
        }
    }

    @Override
    public void deleteTbGoods(Long[] ids) {
        for (Long id : ids) {
            tbGoodsMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbGoods getById(Long id) {
        return tbGoodsMapper.selectByPrimaryKey(id);
    }
}
