package com.qianxi.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.qianxi.entity.PageResult;
import com.qianxi.mapper.TbItemMapper;
import com.qianxi.pojo.TbItem;
import com.qianxi.pojo.TbItemExample;
import com.qianxi.service.TbItemService;

import javax.annotation.Resource;
import java.util.List;

@Service(timeout = 5000,retries = 0)
public class TbItemServiceImpl implements TbItemService {
    @Resource
    private TbItemMapper tbItemMapper;

    @Override
    public List<TbItem> findAll() {
        List<TbItem> tbItems = tbItemMapper.selectByExample(null);
        return tbItems;
    }

    @Override
    public PageResult<TbItem> pageList(TbItem tbItem, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum,pageSize);

        TbItemExample example=new TbItemExample();
        TbItemExample.Criteria criteria = example.createCriteria();
        if(tbItem!=null){

        }
        Page<TbItem> page = (Page<TbItem>) tbItemMapper.selectByExample(example);
        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void saveTbItem(TbItem tbGoods) {
        Long id = tbGoods.getId();
        if(id==null||id==0){
            tbItemMapper.insert(tbGoods);
        }else{
            tbItemMapper.updateByPrimaryKey(tbGoods);
        }
    }

    @Override
    public void deleteTbItem(Long[] ids) {
        for (Long id : ids) {
            tbItemMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbItem getById(Long id) {
        return tbItemMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<TbItem> getByGoodsId(Long id) {

        TbItemExample example=new TbItemExample();
        TbItemExample.Criteria criteria = example.createCriteria();
        criteria.andGoodsIdEqualTo(id);

        List<TbItem> tbItems = tbItemMapper.selectByExample(example);
        return tbItems;
    }
}
