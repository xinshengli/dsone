package com.qianxi.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mysql.jdbc.StringUtils;
import com.qianxi.mapper.TbDistrictMapper;
import com.qianxi.pojo.TbDistrict;
import com.qianxi.pojo.TbDistrictExample;
import com.qianxi.service.TbDistrictService;
import com.qianxi.entity.PageResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbDistrictServiceImpl implements TbDistrictService {

    @Resource
    private TbDistrictMapper tbDistrictMapper;

    @Override
    public List<TbDistrict> findAll() {
        return tbDistrictMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbDistrict> pageList(TbDistrict tbDistrict, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbDistrictExample example = new TbDistrictExample();
        TbDistrictExample.Criteria criteria = example.createCriteria();
        if(tbDistrict !=null){

        }

        Page<TbDistrict> page = (Page<TbDistrict>) tbDistrictMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbDistrict tbDistrict) {
        String id =tbDistrict.getId();
        if(id == null || id.equals("")){
            tbDistrictMapper.insert(tbDistrict);
        } else{
            tbDistrictMapper.updateByPrimaryKey(tbDistrict);
        }
    }

    @Override
    public void deletebth(String[] ids) {
        for (String id : ids) {
            tbDistrictMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbDistrict getById(String id) {
        return tbDistrictMapper.selectByPrimaryKey(id);
    }
}
