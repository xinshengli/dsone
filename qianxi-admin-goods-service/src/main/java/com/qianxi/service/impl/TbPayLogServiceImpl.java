package com.qianxi.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mysql.jdbc.StringUtils;
import com.qianxi.mapper.TbPayLogMapper;
import com.qianxi.pojo.TbPayLog;
import com.qianxi.pojo.TbPayLogExample;
import com.qianxi.service.TbPayLogService;
import com.qianxi.entity.PageResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbPayLogServiceImpl implements TbPayLogService {

    @Resource
    private TbPayLogMapper tbPayLogMapper;

    @Override
    public List<TbPayLog> findAll() {
        return tbPayLogMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbPayLog> pageList(TbPayLog tbPayLog, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbPayLogExample example = new TbPayLogExample();
        TbPayLogExample.Criteria criteria = example.createCriteria();
        if(tbPayLog !=null){

        }

        Page<TbPayLog> page = (Page<TbPayLog>) tbPayLogMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbPayLog tbPayLog) {
        String id =tbPayLog.getOutTradeNo();
        if(id == null || id.equals("")){
            tbPayLogMapper.insert(tbPayLog);
        } else{
            tbPayLogMapper.updateByPrimaryKey(tbPayLog);
        }
    }

    @Override
    public void deletebth(String[] ids) {
        for (String id : ids) {
            tbPayLogMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbPayLog getById(String id) {
        return tbPayLogMapper.selectByPrimaryKey(id);
    }
}
