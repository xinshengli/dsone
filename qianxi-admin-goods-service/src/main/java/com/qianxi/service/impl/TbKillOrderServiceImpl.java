package com.qianxi.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mysql.jdbc.StringUtils;
import com.qianxi.mapper.TbKillOrderMapper;
import com.qianxi.pojo.TbKillOrder;
import com.qianxi.pojo.TbKillOrderExample;
import com.qianxi.service.TbKillOrderService;
import com.qianxi.entity.PageResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbKillOrderServiceImpl implements TbKillOrderService {

    @Resource
    private TbKillOrderMapper tbKillOrderMapper;

    @Override
    public List<TbKillOrder> findAll() {
        return tbKillOrderMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbKillOrder> pageList(TbKillOrder tbKillOrder, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbKillOrderExample example = new TbKillOrderExample();
        TbKillOrderExample.Criteria criteria = example.createCriteria();
        if(tbKillOrder !=null){

        }

        Page<TbKillOrder> page = (Page<TbKillOrder>) tbKillOrderMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbKillOrder tbKillOrder) {
        Long id =tbKillOrder.getId();
        if(id == null || id == 0){
            tbKillOrderMapper.insert(tbKillOrder);
        } else{
            tbKillOrderMapper.updateByPrimaryKey(tbKillOrder);
        }
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbKillOrderMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbKillOrder getById(Long id) {
        return tbKillOrderMapper.selectByPrimaryKey(id);
    }
}
