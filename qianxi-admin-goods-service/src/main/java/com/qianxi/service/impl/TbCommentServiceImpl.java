package com.qianxi.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mysql.jdbc.StringUtils;
import com.qianxi.mapper.TbCommentMapper;
import com.qianxi.pojo.TbComment;
import com.qianxi.pojo.TbCommentExample;
import com.qianxi.service.TbCommentService;
import com.qianxi.entity.PageResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbCommentServiceImpl implements TbCommentService {

    @Resource
    private TbCommentMapper tbCommentMapper;

    @Override
    public List<TbComment> findAll() {
        return tbCommentMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbComment> pageList(TbComment tbComment, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbCommentExample example = new TbCommentExample();
        TbCommentExample.Criteria criteria = example.createCriteria();
        if(tbComment !=null){

        }

        Page<TbComment> page = (Page<TbComment>) tbCommentMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbComment tbComment) {
        Long id =tbComment.getId();
        if(id == null || id == 0){
            tbCommentMapper.insert(tbComment);
        } else{
            tbCommentMapper.updateByPrimaryKey(tbComment);
        }
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbCommentMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbComment getById(Long id) {
        return tbCommentMapper.selectByPrimaryKey(id);
    }
}
