package com.qianxi.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mysql.jdbc.StringUtils;
import com.qianxi.mapper.TbSpecificationOptionMapper;
import com.qianxi.pojo.TbSpecificationOption;
import com.qianxi.pojo.TbSpecificationOptionExample;
import com.qianxi.service.TbSpecificationOptionService;
import com.qianxi.entity.PageResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbSpecificationOptionServiceImpl implements TbSpecificationOptionService {

    @Resource
    private TbSpecificationOptionMapper tbSpecificationOptionMapper;

    @Override
    public List<TbSpecificationOption> findAll() {
        return tbSpecificationOptionMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbSpecificationOption> pageList(TbSpecificationOption tbSpecificationOption, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbSpecificationOptionExample example = new TbSpecificationOptionExample();
        TbSpecificationOptionExample.Criteria criteria = example.createCriteria();
        if(tbSpecificationOption !=null){

        }

        Page<TbSpecificationOption> page = (Page<TbSpecificationOption>) tbSpecificationOptionMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbSpecificationOption tbSpecificationOption) {
        Long id =tbSpecificationOption.getId();
        if(id == null || id == 0){
            tbSpecificationOptionMapper.insert(tbSpecificationOption);
        } else{
            tbSpecificationOptionMapper.updateByPrimaryKey(tbSpecificationOption);
        }
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbSpecificationOptionMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbSpecificationOption getById(Long id) {
        return tbSpecificationOptionMapper.selectByPrimaryKey(id);
    }
}
