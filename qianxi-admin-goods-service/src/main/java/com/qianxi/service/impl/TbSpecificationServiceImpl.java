package com.qianxi.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mysql.jdbc.StringUtils;
import com.qianxi.mapper.TbSpecificationMapper;
import com.qianxi.pojo.TbSpecification;
import com.qianxi.pojo.TbSpecificationExample;
import com.qianxi.service.TbSpecificationService;
import com.qianxi.entity.PageResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbSpecificationServiceImpl implements TbSpecificationService {

    @Resource
    private TbSpecificationMapper tbSpecificationMapper;

    @Override
    public List<TbSpecification> findAll() {
        return tbSpecificationMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbSpecification> pageList(TbSpecification tbSpecification, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbSpecificationExample example = new TbSpecificationExample();
        TbSpecificationExample.Criteria criteria = example.createCriteria();
        if(tbSpecification !=null){

        }

        Page<TbSpecification> page = (Page<TbSpecification>) tbSpecificationMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbSpecification tbSpecification) {
        Long id =tbSpecification.getId();
        if(id == null || id == 0){
            tbSpecificationMapper.insert(tbSpecification);
        } else{
            tbSpecificationMapper.updateByPrimaryKey(tbSpecification);
        }
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbSpecificationMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbSpecification getById(Long id) {
        return tbSpecificationMapper.selectByPrimaryKey(id);
    }
}
