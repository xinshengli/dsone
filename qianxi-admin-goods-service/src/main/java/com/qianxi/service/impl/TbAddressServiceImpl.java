package com.qianxi.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mysql.jdbc.StringUtils;
import com.qianxi.mapper.TbAddressMapper;
import com.qianxi.pojo.TbAddress;
import com.qianxi.pojo.TbAddressExample;
import com.qianxi.service.TbAddressService;
import com.qianxi.entity.PageResult;

import javax.annotation.Resource;
import java.util.List;

/**
 * @描述
 * @创建人 wangyue
 * @创建时间2020/12/811:01
 */
@Service(timeout = 5000,retries = 0)
public class TbAddressServiceImpl implements TbAddressService {

    @Resource
    private TbAddressMapper tbAddressMapper;

    @Override
    public List<TbAddress> findAll() {
        return tbAddressMapper.selectByExample(null);
    }

    @Override
    public PageResult<TbAddress> pageList(TbAddress tbAddress, int pageSize, int pageNum) {
        PageHelper.startPage(pageNum,pageSize);
        TbAddressExample example = new TbAddressExample();
        TbAddressExample.Criteria criteria = example.createCriteria();
        if(tbAddress !=null){

        }

        Page<TbAddress> page = (Page<TbAddress>) tbAddressMapper.selectByExample(example);

        return new PageResult<>(page.getTotal(),page.getResult());
    }

    @Override
    public void save(TbAddress tbAddress) {
        Long id =tbAddress.getId();
        if(id == null || id == 0){
            tbAddressMapper.insert(tbAddress);
        } else{
            tbAddressMapper.updateByPrimaryKey(tbAddress);
        }
    }

    @Override
    public void deletebth(Long[] ids) {
        for (Long id : ids) {
            tbAddressMapper.deleteByPrimaryKey(id);
        }
    }

    @Override
    public TbAddress getById(Long id) {
        return tbAddressMapper.selectByPrimaryKey(id);
    }
}
